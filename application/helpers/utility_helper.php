<?php

function get_data_for_mysql_format($date) {
	list($dia, $mes, $ano) = explode('/', $date);

	return "$ano-$mes-$dia";
}


function format_data_mysql($date) {
	list($data, $hora) = explode(' ', $date);

	list($ano, $mes, $dia) = explode('-', $data);

	return "$dia/$mes/$ano";
}

function format_data_from_mysql($data) {
	list($ano, $mes, $dia) = explode('-', $data);

	return "$dia/$mes/$ano";
}

function format_hora_from_mysql($hora) {
	list($hora, $min, $sec) = explode(':', $hora);

	return "$hora:$min";
}