<?php
// url
$url = ((($this->session->userdata('tipo')) && ($this->session->userdata('tipo') == 'cliente')) ? 'cliente' : 'dashboard');
?>
<div class="col-lg-10 col-md-10">
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong>
			<small>Usuários</small>
		</div>
	</div>
	<h1>Comentários</h1>
	<hr>
	<br>
	
	<?php if ($comentarios != NULL): ?>
		<?php foreach ($comentarios as $comentario): ?>
			<div class="panel panel-default">
	            <div class="panel-heading">
	                <h3 class="panel-title"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Cliente: <?= $comentario->cliente; ?></h3>
	            </div>
	            <div class="panel-body">
	                <div class="dashboard-text">
	                    <p>
	                        <?= $comentario->comentario; ?>
	                    </p>
	                </div>
	            </div>
	        </div>
	    <?php endforeach ?>
    <?php endif ?>
	<hr>
	<form action="<?php echo site_url(''.$url.'/Usuario/comentarios'); ?>" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-12">
				<label for="comentario">Novo comentário</label>
				<textarea name="comentario" id="comentario" class="form-control"></textarea>
			</div>			
		</div>
		
		<div class="row">
			<!-- submit -->
			<div class="col-md-12">
				<div class="form-group">
					<br>
					<input type="submit" class="btn btn-primary" value="Salvar">
					<input type="hidden" name="idUsuario" value="<?php echo $this->uri->segment(4); ?>">
				</div>
			</div>
		</div>
	</form>
</div>