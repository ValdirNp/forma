<?php
	// url
$url = ((($this->session->userdata('tipo')) && ($this->session->userdata('tipo') == 'cliente')) ? 'cliente' : 'dashboard');

	// id
$idCarro   = (isset($servico['result'][0]->idCarro)   ? $servico['result'][0]->idCarro   : '');
$idUsuario = (isset($servico['result'][0]->idUsuario) ? $servico['result'][0]->idUsuario : '');

	// carro
$marca 	= (isset($servico['result'][0]->marca) 	? $servico['result'][0]->marca 	: NULL);
$placa 	= (isset($servico['result'][0]->placa) 	? $servico['result'][0]->placa 	: NULL);
$ano 	= (isset($servico['result'][0]->ano) 	? $servico['result'][0]->ano 	: NULL);
$cor 	= (isset($servico['result'][0]->cor) 	? $servico['result'][0]->cor 	: NULL);
$km 	= (isset($servico['result'][0]->km) 	? $servico['result'][0]->km 	: NULL);
$kmMes 	= (isset($servico['result'][0]->kmMes) 	? $servico['result'][0]->kmMes 	: NULL);

?>

<div class="col-lg-10 col-md-10">
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong>
			<small>Usuários</small>
		</div>
	</div>
	<h1>Cadastrar Carro</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit expedita dicta doloremque, ea, repellendus voluptatem, harum neque minus quisquam hic perferendis commodi suscipit amet. Doloremque ut, alias animi. Animi, consequatur.</p>
	<br>
	<br>

	<form action="<?php echo site_url(''.$url.'/Usuario/carro'); ?>" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label for="marca">Marca</label>
					<select name="marca" id="marca" class="form-control">
						<option value="">Selecione...</option>
						<?php foreach ($marcas as $each): ?>
							<option value="<?= $each->nome; ?>" data-id="<?= $each->id; ?>"><?= $each->nome; ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="modelo">Modelo</label>
					<select name="modelo" id="modelo" class="form-control">
						<option value="">Selecione...</option>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="placa">Placa</label>
					<input type="text" name="placa" class="form-control" placeholder="Placa" id="placa" value="<?php echo $placa; ?>">
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="ano">Ano</label>
					<select name="ano" id="ano" class="form-control">
						<option value="">Selecione...</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label for="cor">Cor</label>
					<input type="text" name="cor" class="form-control" placeholder="Cor" id="cor" value="<?php echo $cor; ?>">
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="km">Km</label>
					<input type="text" name="km" class="form-control" placeholder="Km" id="km" value="<?php echo $km; ?>">
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="kmMes">Km/Mês</label>
					<input type="text" name="kmMes" class="form-control" placeholder="kmMes" id="kmMes" value="<?php echo $kmMes; ?>">
				</div>
			</div>
		</div>
		<div class="row">
			<!-- submit -->
			<div class="col-md-12">
				<div class="form-group">
					<br>
					<input type="submit" class="btn btn-primary" value="<?php echo (($idCarro != '') ? 'Atualizar' : 'Cadastrar'); ?>">
					<input type="hidden" name="idUsuario" value="<?php echo $this->uri->segment(4); ?>">
				</div>
			</div>
		</div>
	</form>

	<div class="col-md-12" style="padding: 0;">
		<div class="form-group">
			<hr>
		</div>
	</div>

	<div class="col-md-12" style="padding: 0;">
		<div class="form-group">
			<!-- success -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<span class="badge"><?php echo $lista['numrows']; ?></span> - Carro(s)
				</div>
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Marca</th>
							<th>Modelo</th>
							<th>Placa</th>
							<th>Ano</th>
							<th>Cor</th>
							<th>Km</th>
							<th>Km/Mês</th>
							<th>dataHora</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						for ($i=0; $i < count($lista['result']); $i++) {
							?>
							<tr>
								<td><?php echo $lista['result'][$i]->idCarro; ?></td>
								<td><?php echo $lista['result'][$i]->marca; ?></td>
								<td><?php echo $lista['result'][$i]->modelo; ?></td>
								<td><?php echo $lista['result'][$i]->placa; ?></td>
								<td><?php echo $lista['result'][$i]->ano; ?></td>
								<td><?php echo $lista['result'][$i]->cor; ?></td>
								<td><?php echo $lista['result'][$i]->km; ?></td>
								<td><?php echo $lista['result'][$i]->kmMes; ?></td>
								<td><?php echo $lista['result'][$i]->dataHora; ?></td>
								<td class="text-right">
									<a href="<?php echo site_url('dashboard/Usuario/carroDelete') .'/?carro='. $lista['result'][$i]->idCarro .'&usuario='. $lista['result'][$i]->idUsuario; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
	$('#marca').change(function(){
		var id = $('#marca option:selected').data('id');
		$.ajax({
			url : '<?= site_url("dashboard/Usuario/get_modelos"); ?>',
			type : 'POST',
			data : {
				id: id
			},
			success : function(data){
				console.log(data)
				$('#modelo').html(data);
			}
		});
	});

	$('#modelo').change(function(){
		var id = $('#modelo option:selected').data('id');
		$.ajax({
			url : '<?= site_url("dashboard/Usuario/get_anos"); ?>',
			type : 'POST',
			data : {
				id: id
			},
			success : function(data){
				console.log(data)
				$('#ano').html(data);
			}
		});
	});
</script>