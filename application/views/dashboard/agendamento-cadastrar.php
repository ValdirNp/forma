<div class="col-lg-10">
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong>
			<small>Agendamentos</small>
		</div>
	</div>
	<form action="<?php echo site_url('dashboard/Agendamento/cadastrar'); ?>" method="post">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center">Empresa</th>
					<th class="text-center">Serviço</th>
					<th class="text-center">Data</th>
					<th class="text-center">Horário & Nº de Atendimentos</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<select name="idComercio" id="idComercio" class="form-control">
							<?php
							for ($i=0; $i < count($dropdownDeComercio['result']); $i++) { 
								?>
								<option value="<?php echo $dropdownDeComercio['result'][$i]->idComercio; ?>"><?php echo $dropdownDeComercio['result'][$i]->razao; ?></option>
								<?php
							}
							?>
						</select>
					</td>
					<td>
						<select name="idServico" id="idServico" class="form-control">
							
						</select>
						<a href="<?php echo site_url('dashboard/Servico/cadastrar'); ?>" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> Serviços </a>
					</td>
					<td style="width: 150px;" >
						<input id="data" type="text" name="data" class="form-control">
					</td>
					<td style="width: 250px; padding: 0; text-align: center;">
						<table class="table table-bordered" style="margin-bottom: 0;">
							<tr>
								<td>
									<input type="time" name="horario[]" class="form-control" style="width: 100px;">
								</td>
								<td>
									<select name="numeroDeAtendimento[]" class="form-control">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
								</td>
								<td>
									<a href="#" class="btn btn-success addTr"><span class="glyphicon glyphicon-plus"></span></a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="4"><input type="submit" class="btn btn-primary" value="Cadastrar" id="submit"></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>
<script>
	$('#data').multiDatesPicker({
		dateFormat: "dd/mm/yy",
		monthNames: [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ],
		dayNamesMin: [ "Do", "Se", "Te", "Qu", "Qu", "Se", "Sa" ],
	});

	$('#idComercio').change(function(event) {
		idComercio = $(this).val();
		$.ajax({
			url: '<?= site_url("dashboard/Agendamento/busca_servicos"); ?>',
			type: 'POST',
			data: {
				id: idComercio
			},
			success: function(data){
				$('#idServico').html(data);
			}
		});		
	});


</script>