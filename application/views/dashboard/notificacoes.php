<div class="col-md-10">
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-warning-sign"></span> <strong>Últimas notificações</strong>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-calendar"></span> Agendamentos</div>
				<table class="table">
					<tr>
						<td><b>Ref</b></td>
						<td><b>User</b></td>
						<td><b>Cliente</b></td>
						<td><b>Data</b></td>
						<!-- <td><b>Serviço</b></td> -->
						<td><b>Status</b></td>
					</tr>
					<?php 
						$i=0;
					foreach ($agendamentos as $agendamento) { ?>
						<tr>
							<td><?= $i; ?></td>
							<td><?= $agendamento->nome ?></td>
							<td><?= $agendamento->cliente ?></td>
							<td><?= date('d/m/Y', strtotime($agendamento->data_agendamento )) ?> - <?= $agendamento->hora_agendamento ?></td>
							<!-- <td>Troca de pneu</td> -->
							<td class="confirmed"><?= $agendamento->status ?></td>
						</tr>
					<?php $i++;
							}
					  ?>
				</table>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-user"></span> Clientes</div>
				<table class="table">
					<tr>
						<td><b>Quem cadastrou</b></td>
						<td><b>Nome</b></td>
						<td><b>Data</b></td>
					</tr>
					<?php foreach ($clientes as $cliente) { ?>
						<tr>
						<td><?= $cliente->quem_cadastrou ?></td>
						<td><?= $cliente->cliente_cadastrado ?></td>
						<td><?= date('d/m/Y - H:i', strtotime($cliente->data)) ?></td>
						</tr>
					<?php }  ?>
				</table>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-user"></span> Avaliações</div>
				<table class="table">
					<tr>
						<td><b>Usuário</b></td>
						<td><b>Cliente</b></td>
						<td><b>Data</b></td>
						<td><b>Comentário</b></td>
					</tr>
					<?php foreach ($avaliacoes as $avaliacao) { ?>
							
					<tr>
						<td><?= $avaliacao->usuario ?></td>
						<td><?= $avaliacao->cliente ?></td>
						<td><?= date('d/m/Y - H:i', strtotime($avaliacao->data)) ?></td>
						<td><?= $avaliacao->comentario ?></td>
					</tr>
					<?php }  ?>

				</table>
			</div>
		</div>
	</div>
</div>