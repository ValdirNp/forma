<?php $url = ((($this->session->userdata('tipo')) && ($this->session->userdata('tipo') == 'cliente')) ? 'cliente' : 'dashboard'); ?>
<div class="col-lg-10 col-md-10">
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong>
			<small>Clientes</small>
		</div>
	</div>
	<div class="row mb-20">
		<div class="col-lg-5">
			<form class="form-inline">
				<div class="form-group">
					<label>Pesquisar</label>
					<input type="text" class="form-control">
					<input type="submit" value="OK" class="btn btn-primary">
				</div>
			</form>
		</div>
	</div>
	<div class="panel panel-success">
		<div class="panel-heading">
			<span class="badge"><?php echo $listaDeAtivos['numrows']; ?></span> - Aprovado(s)
			<a href="<?php echo site_url('dashboard/Cliente/cadastrar'); ?>" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> Clientes </a>
			<br clear="all">
		</div>
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Cliente</th>
					<th>Tipo</th>
					<th class="hidden-xs">Agregados</th>
					<th class="hidden-xs">DataHora</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php for ($i=0; $i < count($listaDeAtivos['result']); $i++) { ?>
					<tr>
						<td><?php echo $listaDeAtivos['result'][$i]->idComercio; ?></td>
						<td><?php echo $listaDeAtivos['result'][$i]->razao; ?></td>
						<td><?php echo $listaDeAtivos['result'][$i]->nivel; ?></td>
						<td class="hidden-xs">0</td>
						<td class="hidden-xs"><?php echo $listaDeAtivos['result'][$i]->dataHora ?></td>
						<td class="text-right"><a href="<?php echo site_url('dashboard/Cliente/visualizar') .'/'. $listaDeAtivos['result'][$i]->idUsuario; ?>"><span class="glyphicon glyphicon-eye-open"></span></a></td>
						<!-- <td><a href="<?php echo site_url(''.$url.'/Usuario/comercio') .'/'. $listaDeAtivos['result'][$i]->idUsuario; ?>"><span class="glyphicon glyphicon-wrench"></span></a></td> -->
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>

	<!-- warning -->
	<div class="panel panel-warning">
		<div class="panel-heading"><span class="badge"><?php echo $listaDePendentes['numrows']; ?></span> - Pendente(s)</div>
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Cliente</th>
					<th>Tipo</th>
					<th class="hidden-xs">Agregados</th>
					<th class="hidden-xs">DataHora</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
				for ($i=0; $i < count($listaDePendentes['result']); $i++) {
					?>
					<tr>
						<td><?php echo $listaDePendentes['result'][$i]->idUsuario; ?></td>
						<td><?php echo $listaDePendentes['result'][$i]->razao; ?></td>
						<td><?php echo $listaDePendentes['result'][$i]->nivel; ?></td>
						<td class="hidden-xs">0</td>
						<td class="hidden-xs"><?php echo $listaDePendentes['result'][$i]->dataHora; ?></td>
						<td class="text-right">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									Ação <span class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu">
									<li><a href="<?php echo site_url('dashboard/Cliente/visualizar') .'/'. $listaDePendentes['result'][$i]->idUsuario; ?>">Visualizar</a></li>
								</ul>
							</div>
						</td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
	</div>
</div>