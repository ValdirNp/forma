<?php 
	// url
$url = ((($this->session->userdata('tipo')) && ($this->session->userdata('tipo') == 'cliente')) ? 'cliente' : 'dashboard');
?>

<div class="col-lg-10 col-md-10">
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong>
			<small>Usuários</small>
		</div>
	</div>
	<h1>Cadastrar Comércio</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit expedita dicta doloremque, ea, repellendus voluptatem, harum neque minus quisquam hic perferendis commodi suscipit amet. Doloremque ut, alias animi. Animi, consequatur.</p>
	<br>
	<br>
	<div class="row">
		<form action="<?php echo site_url(''.$url.'/Usuario/comercio'); ?>" method="post" enctype="multipart/form-data">
			<div class="col-md-10">
				<div class="form-group">
					<label for="marca">Comércio</label>
					<select name="idComercio" id="idComercio" class="form-control">
						<option value="">Selecione</option>
						<?php
						for ($i=0; $i < count($comercio['result']); $i++) { 
							if ($comercio['result'][$i]->razao != '') {
								echo '<option value="'.$comercio['result'][$i]->idComercio.'">'.$comercio['result'][$i]->razao.'</option>';
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label style="visibility: hidden;">Comércio</label>
					<input type="submit" class="btn btn-primary" value="Cadastrar" style="width: 100%;">
					<input type="hidden" name="idUsuario"  value="<?php echo $this->uri->segment(4); ?>">
				</div>
			</div>
		</form>
	</div>

	<div class="col-md-12" style="padding: 0;">
		<div class="form-group">
			<br>
			<hr>
			<br>
		</div>
	</div>

	<div class="col-md-12" style="padding: 0;">
		<div class="form-group">
			<!-- success -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<span class="badge"><?php echo $lista['numrows']; ?></span> - Comércio(s)
				</div>
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>cnpj</th>
							<th>razao</th>
							<th>telefone</th>
							<th>celular</th>
						</tr>
					</thead>
					<tbody>
						<?php
						for ($i=0; $i < count($lista['result']); $i++) { 
							?>
							<tr>
								<td><?php echo $lista['result'][$i]->id; ?></td>
								<td><?php echo $lista['result'][$i]->cnpj; ?></td>
								<td><?php echo $lista['result'][$i]->razao; ?></td>
								<td><?php echo $lista['result'][$i]->telefone; ?></td>
								<td><?php echo $lista['result'][$i]->celular; ?></td>
								<td class="text-right">
									<a href="<?php echo site_url(''.$url.'/Usuario/comercioDelete') .'/comercio/?comerdio='. $lista['result'][$i]->id .'&usuario='. $this->uri->segment(4); ?>" class="btn btn-danger">
										<span class="glyphicon glyphicon-trash"></span>
									</a>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>