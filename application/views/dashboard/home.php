<div class="col-md-10 contadores ">
	<div class="row mb-20">				
		<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
			<h2><span class="label label-success"><?= $agendamentos['numrows']; ?></span></h2><b>agendamentos</b>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
			<h2><span class="label label-success"><?= $clientes['numrows']; ?></span></h2><b>clientes</b>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
			<h2><span class="label label-success"><?= $usuarios['numrows']; ?></span></h2><b>usuários</b>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
			<h2><span class="label label-success"><?= count($avaliacoes); ?></span></h2><b>avaliações</b>
		</div>
		<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 pull-right alertas">
			<a href="<?= site_url('dashboard/Notificacoes'); ?>">
				<h2><span class="label label-danger"><?=$notificacoes?></span></h2><b>alertas</b>
			</a>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong> <small>Aqui um breve resumo do seu site</small>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-8 col-md-8">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-calendar"></span> Últimos agendamentos</div>
				<table class="table">
					<tr>
						<td><b>Ref</b></td>
						<td><b>User</b></td>
						<td><b>Cliente</b></td>
						<td><b>Data</b></td>
						<td><b>Serviço</b></td>
						<td><b>Status</b></td>
					</tr>
					<?php $limit = count($lista); ?>
					<?php if ($limit > 5) $limit = 5; ?>
					<?php for ($i=0; $i < $limit; $i++): ?> 
						<tr>
							<td><?= $lista[$i]['id']; ?></td>
							<td><?= $lista[$i]['nome']; ?></td>
							<td><?= $lista[$i]['comercio']; ?></td>
							<td><?= $lista[$i]['data']; ?> - <?= $lista[$i]['horario']; ?></td>
							<td><?= $lista[$i]['servico']; ?></td>
							<td class="<?= $lista[$i]['class']; ?>"><?= $lista[$i]['status']; ?></td>
						</tr>
					<?php endfor ?>
				</table>
			</div>
		</div>
		<div class="col-lg-4 col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-user"></span> Últimos usuários</div>
				<table class="table">
					<tr>
						<td><b>User</b></td>
						<td><b>Cliente</b></td>
						<td><b>Data</b></td>
					</tr>
					<?php $limit = count($usuarios['result']); ?>
					<?php if ($limit > 5) $limit = 5; ?>
					<?php for ($i=0; $i < $limit; $i++): ?>
						<tr>
							<td><?= $usuarios['result'][$i]->nome; ?></td>
							<td><?= $usuarios['result'][$i]->cliente_cad; ?></td>
							<td><?= $usuarios['result'][$i]->dataHora; ?></td>
						</tr>
					<?php endfor ?>
				</table>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-calendar"></span> Agendamentos por período</div>
				<canvas class="panel-body" id="agend_periodo" style="width:426px; height:200px">
				</canvas>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-user"></span> Cadastro de usuários</div>
				<canvas class="panel-body" id="usuarios_periodo" style="width:426px; height:200px">
				</canvas>
			</div>
		</div>
	</div>
</div>
<script>
	var chart_agendamento = document.getElementById('agend_periodo').getContext("2d");
	var chart_periodo = document.getElementById('usuarios_periodo').getContext("2d");
	var data_agend = <?= $agendamentoPeriodo ?>;
	var data_periodo = <?= $usersPeriodo ?>;


	var lineOptions = {
	    scaleShowGridLines : true,	//Boolean - Whether grid lines are shown across the chart
	    scaleGridLineColor : "rgba(0,0,0,.05)",	//String - Colour of the grid lines
	    scaleGridLineWidth : 1,	//Number - Width of the grid lines
	    scaleShowHorizontalLines: true,	//Boolean - Whether to show horizontal lines (except X axis)
	    scaleShowVerticalLines: true,	//Boolean - Whether to show vertical lines (except Y axis)
	    bezierCurve : false,	//Boolean - Whether the line is curved between points
	    bezierCurveTension : 0.4,	//Number - Tension of the bezier curve between points
	    pointDot : true,	//Boolean - Whether to show a dot for each point
	    pointDotRadius : 4,	//Number - Radius of each point dot in pixels
	    pointDotStrokeWidth : 1,	//Number - Pixel width of point dot stroke
	    pointHitDetectionRadius : 20,	//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
	    datasetStroke : true,	//Boolean - Whether to show a stroke for datasets
	    datasetStrokeWidth : 2,	//Number - Pixel width of dataset stroke
	    datasetFill : true,	//Boolean - Whether to fill the dataset with a colour
	    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"	//String - A legend templ

	};

	new Chart(chart_agendamento).Line(data_agend, lineOptions);
	new Chart(chart_periodo).Line(data_periodo, lineOptions);

	
</script>