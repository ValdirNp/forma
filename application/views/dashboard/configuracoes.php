<div class="col-lg-10 col-md-10">
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong>
			<small>Configurações</small>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4 mb-30">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-pencil"></span> Plano 1</div>
				<div class="panel-body">
					<h3 class="title mb-20">Plano 1</h3>
					<form action="#">
						<div class="mb-20">
							<textarea name="" rows="4" class="form-control">A vantagem desse plano é porque ele é o 1.</textarea>
						</div>
						<label for="valor1">Valor atual:</label>
						<div class="row">
							<div class="col-sm-7">
								<input type="text" id="valor1" value="R$ 100,00" class="form-control mb-10 mask-money" />
							</div>
							<div class="col-sm-5">
								<input type="submit" class="btn btn-info btn-block" value="Atualizar" >
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="col-lg-4 mb-30">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-pencil"></span> Plano 2</div>
				<div class="panel-body">
					<h3 class="title mb-20">Plano 2</h3>
					<form action="#">
						<div class="mb-20">
							<textarea name="" rows="4" class="form-control">A vantagem desse plano é porque ele é o 2.</textarea>
						</div>
						<label for="valor1">Valor atual:</label>
						<div class="row">
							<div class="col-sm-7">
								<input type="text" id="valor1" value="R$ 200,00" class="form-control mb-10 mask-money" />
							</div>
							<div class="col-sm-5">
								<input type="submit" class="btn btn-info btn-block" value="Atualizar" >
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="col-lg-4 mb-30">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-pencil"></span> Plano 3</div>
				<div class="panel-body">
					<h3 class="title mb-20">Plano 3</h3>
					<form action="#">
						<div class="mb-20">
							<textarea name="" rows="4" class="form-control">A vantagem desse plano é porque ele é o 3.</textarea>
						</div>
						<label for="valor1">Valor atual:</label>
						<div class="row">
							<div class="col-sm-7">
								<input type="text" id="valor1" value="R$ 300,00" class="form-control mb-10 mask-money" />
							</div>
							<div class="col-sm-5">
								<input type="submit" class="btn btn-info btn-block" value="Atualizar" >
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	
	<hr class="mt-30 mb-30">

	<!-- FILTRO -->
	<div class="dashboard-filter mb-30">
		<div class="row">
			<div class="col-sm-5">
				<form action="#">
					<label for="pesquisar">Pesquisar</label>
					<div class="row">
						<div class="col-sm-9">
							<input type="text" id="pesquisar" class="form-control" />
						</div>
						<div class="col-sm-3">
							<input type="submit" value="OK" class="btn btn-info btn-block" />
						</div>
					</div>
				</form>
			</div>
			<div class="col-sm-7">
				<div class="mb-5">
					<label for="#">Organizar por:</label>
				</div>
				<label class="radio-inline">
					<input type="radio" name="ordem" id="ord_cliente" value="cliente"> Cliente
				</label>
				<label class="radio-inline">
					<input type="radio" name="ordem" id="ord_usuario" value="usuario"> Usuário
				</label>
				<label class="radio-inline">
					<input type="radio" name="ordem" id="ord_destaque" value="destaque"> Destaque
				</label>
			</div>
		</div>
	</div>

	<!-- USUARIOS -->
	<div class="dashboard-box mb-30">

		<div class="dashboard-box-head"><span class="glyphicon glyphicon-user"></span> Usuários cadastrados <a href="<?= site_url('dashboard/Usuario'); ?>" class="pull-right">ver lista completa</a></div>

		<div class="dashbord-box-content">
			<table class="table">
				<thead>
					<tr>
						<th>User</th>
						<th>Email</th>
						<th>Cliente</th>
						<th>Destaque</th>
						<th>Transformar destaque</th>
					</tr>
				</thead>
				<tbody>
					<?php $aux = 0; ?>
					<?php if ($aux < 5): ?>
						<?php foreach ($usuarios['result'] as $usuario): ?>
							<tr  class="dashboard-item-order">
								<td><?= $usuario->nome; ?></td>
								<td><?= $usuario->email; ?></td>
								<td><?= $usuario->cliente; ?></td>
								<td><span><?= $usuario->destaque; ?></span></td>
								<td>
									<form action="#">
										<label class="radio-inline">
											<input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" checked> Não
										</label>
										<label class="radio-inline">
											<input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Sim
										</label>
									</form>
								</td>
							</tr>
							<?php $aux++; ?>
						<?php endforeach ?>
					<?php endif ?>
				</tbody>
			</table>
		</div>
	</div>
</div>