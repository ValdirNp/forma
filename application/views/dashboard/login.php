<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Orbit - Ferramenta Mobile</title>
		
		<!-- meta -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- css -->
		<link rel="stylesheet" href="<?php echo base_url('common/bootstrap/css/bootstrap.min.css'); ?>">
		<link rel="stylesheet" href="<?php echo base_url('common/css/dashboard.css'); ?>">
		
		<!-- js -->
		<script src="<?php echo base_url('common/javascript/jquery.js'); ?>" ></script>	
		<script src="<?php echo base_url('common/bootstrap/js/bootstrap.min.js'); ?>" ></script>
		<script src="<?php echo base_url('common/javascript/dashboard.js'); ?>" ></script>	
	</head>

	<body>
		<div class="site">
			<div class="container-fluid">
				
				<div class="col-md-4 col-md-offset-4 login">
					<div class="col-md-12 text-center">
						<img src="<?php echo base_url('common/image/orbit-ferramentas-mobile.jpg'); ?>" alt="Orbit - Ferramentas Mobile">
					</div>
					<form action="<?php echo site_url('dashboard/Login'); ?>" method="post">
						<!-- flashData -->
						<div class="col-md-12">
							<div class="form-group">
								<?php
									$flashdata = $this->session->flashdata('flashdata');
									if ($flashdata) {
										?>
											<div class="alert <?php echo $flashdata['type']; ?> alert-dismissible" role="alert">
												<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<?php echo $flashdata['msg']; ?>
											</div>
										<?php
									}
								?>
							</div>
						</div>
						<!-- flashData fim -->

						<div class="col-md-12">
							<div class="form-group">
								<label for="email">Email</label>
								<input type="email" name="email" class="form-control" placeholder="E-mail">
							</div>
						</div>
						<div class="col-md-6 col-xs-6">
							<div class="form-group">
								<label for="senha">Senha</label>
								<input type="password" name="senha" class="form-control" placeholder="Senha">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<input type="submit" value="Login" class="btn btn-primary">
							</div>
						</div>
						<br clear="all">
					</form>	
				</div>
			</div>
		</div>
	</body>
</html>

