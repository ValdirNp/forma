<?php
	// usuario
	// var_dump($usuario);
$idUsuario = (isset($usuario['result'][0]->idUsuario) ? $usuario['result'][0]->idUsuario : '');
$nome 	   = (isset($usuario['result'][0]->nome)      ? $usuario['result'][0]->nome 	 : '');
$email     = (isset($usuario['result'][0]->email) 	  ? $usuario['result'][0]->email 	 : '');
$telefone  = (isset($usuario['result'][0]->telefone)  ? $usuario['result'][0]->telefone  : '');
$telEmergencia  = (isset($usuario['result'][0]->telEmergencia)  ? $usuario['result'][0]->telEmergencia  : '');
$nivel     = (isset($usuario['result'][0]->nivel) 	  ? $usuario['result'][0]->nivel 	 : '');
$idStatus  = (isset($usuario['result'][0]->idStatus)  ? $usuario['result'][0]->idStatus  : '');

	// comercio
	// var_dump($comercio);
$idComercio 	   = (isset($comercio['result'][0]->idComercio)    	 ? $comercio['result'][0]->idComercio 	   : '');
$fotoDaEmpresa     = (isset($comercio['result'][0]->fotoDaEmpresa) 	 ? $comercio['result'][0]->fotoDaEmpresa   : '');
$idPlanoEmergencia = (isset($comercio['result'][0]->planoEmergencia) ? $comercio['result'][0]->planoEmergencia : '');
$idPlanoFinanceiro = (isset($comercio['result'][0]->planoFinanceiro) ? $comercio['result'][0]->planoFinanceiro : '');
$idPlanoFidelidade = (isset($comercio['result'][0]->planoFidelidade) ? $comercio['result'][0]->planoFidelidade : '');
$cnpj 			   = (isset($comercio['result'][0]->cnpj) 			 ? $comercio['result'][0]->cnpj 		   : '');
$razao 			   = (isset($comercio['result'][0]->razao) 			 ? $comercio['result'][0]->razao 		   : '');
$descricao 		   = (isset($comercio['result'][0]->descricao) 		 ? $comercio['result'][0]->descricao 	   : '');

	// endereco
	// var_dump($endereco);
$idEndereco  = (isset($endereco['result'][0]->idEndereco)  ? $endereco['result'][0]->idEndereco  : '');
$cep         = (isset($endereco['result'][0]->cep) 		   ? $endereco['result'][0]->cep 		 : '');
$rua    	 = (isset($endereco['result'][0]->endereco)    ? $endereco['result'][0]->endereco 	 : '');
$numero      = (isset($endereco['result'][0]->numero) 	   ? $endereco['result'][0]->numero 	 : '');
$complemento = (isset($endereco['result'][0]->complemento) ? $endereco['result'][0]->complemento : '');
$bairro      = (isset($endereco['result'][0]->bairro) 	   ? $endereco['result'][0]->bairro 	 : '');
$cidade      = (isset($endereco['result'][0]->cidade) 	   ? $endereco['result'][0]->cidade 	 : '');
$estado      = (isset($endereco['result'][0]->estado) 	   ? $endereco['result'][0]->estado 	 : '');
$latitude      = (isset($endereco['result'][0]->latitude) 	   ? $endereco['result'][0]->latitude 	 : '');
$longitude      = (isset($endereco['result'][0]->longitude) 	   ? $endereco['result'][0]->longitude 	 : '');
?>

<div class="col-lg-10 col-md-10">
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong>
			<small>Clientes</small>
		</div>
	</div>
	<h1>Cadastrar Cliente</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit expedita dicta doloremque, ea, repellendus voluptatem, harum neque minus quisquam hic perferendis commodi suscipit amet. Doloremque ut, alias animi. Animi, consequatur.</p>
	<br>
	<br>

	<form action="<?php echo site_url('dashboard/Cliente/cadastrar'); ?>" method="post" enctype="multipart/form-data">
		<div class="col-md-12">
			<div class="form-group">
				<br>
				<input type="button" id="btn-editar" class="btn btn-success procurarCEP" value="Editar">
				<input type="submit" class="btn btn-primary btn-hide" value="<?php echo (isset($usuario) ? 'Atualizar' : 'Cadastrar'); ?>">
			</div>
		</div>
		<!-- dados pessoais -->
		<div class="col-md-12">
			<div class="form-group">
				<h2>Dados Pessoais</h2>
				<hr>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<label for="nome">Nome</label>
				<input type="text" name="nome" class="form-control disable-it" placeholder="Nome Completo" id="nome" value="<?php echo $nome; ?>">
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<label for="email">Email</label>
				<input type="email" name="email" class="form-control" placeholder="Email" id="email" value="<?php echo $email; ?>">
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<label for="telefone">Telefone</label>
				<input type="text" name="telefone" class="form-control" placeholder="(00) 0000-0000" id="telefone" value="<?php echo $telefone; ?>">
			</div>
		</div>

		<div class="col-md-2">
			<div class="form-group">
				<label for="nivel">Nível</label>
				<select name="nivel" class="form-control" id="nivel">
					<option value="">Selecione</option>
					<option value="Root" <?php echo (($nivel == 'Root') ? 'selected' : ''); ?>>Root</option>
					<option value="Cliente" <?php echo (($nivel == 'Cliente') ? 'selected' : ''); ?>>Cliente</option>
				</select>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<label for="idStatus">Status</label>
				<select name="idStatus" class="form-control" id="idStatus">
					<option value="">Selecione</option>
					<?php
					for ($i=0; $i < count($listaDeStatus['result']); $i++) {
						echo '<option value="'. $listaDeStatus['result'][$i]->idStatus .'" '. (($idStatus == $listaDeStatus['result'][$i]->idStatus) ? 'selected' : '') .'>'. $listaDeStatus['result'][$i]->status .'</option>';
					}
					?>
				</select>
			</div>
		</div>

		<!-- dados empresariais -->
		<div class="col-md-12">
			<div class="form-group">
				<h2>Dados Empresariais</h2>
				<hr>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="razao">Razão Social</label>
				<input type="text" name="razao" class="form-control" placeholder="Razão Social" id="razao" value="<?php echo $razao; ?>">
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="cnpj">CNPJ</label>
				<input type="text" name="cnpj" class="form-control" placeholder="CNPJ" id="cnpj" value="<?php echo $cnpj; ?>">
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label for="descricao">Descrição</label>
				<textarea name="descricao" id="descricao" class="form-control" placeholder="Breve descrição" rows="5"><?php echo $descricao; ?></textarea>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label for="descricao">Logo</label>
				<input type="file" name="file" class="form-control">
				<?php if ($fotoDaEmpresa != ''): ?>
					<input type="hidden" name="fotoDaEmpresaHidden" value="<?php echo $fotoDaEmpresa; ?>">
					<a href="<?php echo base_url('common/data/'. $fotoDaEmpresa .''); ?>" target="_blank" style="display: block; margin-top: 5px;">Visualizar arquivo</a>
				<?php endif ?>
			</div>
		</div>

		<!-- endereço -->
		<div class="col-md-12">
			<div class="form-group">
				<h2>Endereço</h2>
				<hr>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label for="cep">CEP</label>
				<input type="text" class="form-control" id="cep" placeholder="00000-000" name="cep" value="<?php echo $cep; ?>">
			</div>
		</div>
		<!-- <div class="col-md-2">
			<div class="form-group">
				<label style="visibility: hidden;">Procurar</label>
				<button class="form-control btn btn-success procurarCEP">Procurar CEP</button>
			</div>
		</div> -->
		<div class="col-md-10">
			<div class="form-group">
				<label for="endereco">Endereço</label>
				<input type="text" class="form-control" id="endereco" placeholder="Endereço" name="endereco" value="<?php echo $rua; ?>">
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<label for="numero">Número</label>
				<input type="text" class="form-control" id="numero" placeholder="Nº" name="numero" value="<?php echo $numero; ?>">
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label for="complemento">Complemento</label>
				<input type="text" class="form-control" id="complemento" placeholder="Complemento" name="complemento" value="<?php echo $complemento; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="bairro">Bairro</label>
				<input type="text" class="form-control" id="bairro" placeholder="Bairro" name="bairro" value="<?php echo $bairro; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="cidade">Cidade</label>
				<input type="text" class="form-control" id="cidade" placeholder="Cidade" name="cidade" value="<?php echo $cidade; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="estado">Estado</label>
				<input type="text" class="form-control" id="estado" placeholder="Estado" name="estado" value="<?php echo $estado; ?>">
			</div>
		</div>
		<input type="hidden" name="latitude" id="latitude" value="<?= $latitude; ?>">
		<input type="hidden" name="longitude" id="longitude" value="<?= $longitude; ?>">

		<!-- planos & serviços -->
		<div class="col-md-12">
			<div class="form-group">
				<h2>Planos & Serviços</h2>
				<hr>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="idPlanoFidelidade">Programa de Fidelidade</label>
				<select name="idPlanoFidelidade" class="form-control" id="idPlanoFidelidade">
					<option value="">Selecione</option>
					<option value="Sim" <?php echo (($idPlanoFidelidade == 'Sim') ? 'selected' : ''); ?>>Sim</option>
					<option value="Não" <?php echo (($idPlanoFidelidade == 'Não') ? 'selected' : ''); ?>>Não</option>
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="idPlanoEmergencia">Participa emergência</label>
				<select name="idPlanoEmergencia" class="form-control" id="idPlanoEmergencia">
					<option value="">Selecione</option>
					<option value="Sim" <?php echo (($idPlanoEmergencia == 'Sim') ? 'selected' : ''); ?>>Sim</option>
					<option value="Não" <?php echo (($idPlanoEmergencia == 'Não') ? 'selected' : ''); ?>>Não</option>
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="idPlanoFinanceiro">Plano Financeiro </label>
				<select name="idPlanoFinanceiro" class="form-control" id="idPlanoFinanceiro">
					<option value="">Selecione</option>
					<option value="Plano A" <?php echo (($idPlanoFinanceiro == 'Plano A') ? 'selected' : ''); ?>>Plano A</option>
					<option value="Plano B" <?php echo (($idPlanoFinanceiro == 'Plano B') ? 'selected' : ''); ?>>Plano B</option>
					<option value="Plano C" <?php echo (($idPlanoFinanceiro == 'Plano C') ? 'selected' : ''); ?>>Plano C</option>
				</select>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="telEmergencia">Telefone de emergência</label>
				<input type="text" name="telEmergencia" class="form-control" placeholder="(00) 0000-0000" id="telEmergencia" value="<?php echo $telEmergencia; ?>">
			</div>
		</div>

		<!-- serviços -->
		<div class="col-md-12">
			<div class="form-group">
				<h2>Serviços do Cliente</h2>
				<hr>
			</div>
		</div>
		<div class="col-md-12">
			<div class="row">
				<?php foreach($servicos['result'] as $servico){ ?>
				<div class="col-sm-4">
					<div class="checkbox">
						<label>
							<input type="checkbox" class="disable-it" name="servicos[]" <?php echo (isset($servicos_cadastrados) && in_array($servico->idServico, $servicos_cadastrados) ? 'checked' : ''); ?> value="<?php echo $servico->idServico ?>">
							<?php echo $servico->servico ?>
						</label>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>

		<!-- serviços -->
		<div class="col-md-12">
			<div class="form-group">
				<h2>Planos do Cliente</h2>
				<hr>
			</div>
		</div>
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-calendar"></span> Últimos pagamentos</div>
				<table class="table">
					<tr>
						<td><b>Data de vencimento</b></td>
						<td><b>Pago</b></td>
					</tr>
					<?php foreach ($pagamentos['result'] as $pagamento): ?>
						<tr>
							<td><?= $pagamento->vencimento; ?></td>
							<td><?= $pagamento->pago; ?></td>
						</tr>
					<?php endforeach ?>
				</table>
			</div>

			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label for="vencimento">Vencimento</label>
						<input type="date" name="vencimento" class="form-control" id="vencimento">
					</div>
				</div>
				<div class="col-sm-4">
					<label for="pago">Pago</label>
					<select name="pago" id="pago" class="form-control">
						<option value="N">Não</option>
						<option value="S">Sim</option>
					</select>
				</div>
			</div>
		</div>

		<!-- submit -->
		<div class="col-md-12">
			<div class="form-group">
				<br>
				<input type="button" id="btn-editar" class="btn btn-success procurarCEP" value="Editar">
				<input type="submit" class="btn btn-primary btn-hide" value="<?php echo (isset($usuario) ? 'Atualizar' : 'Cadastrar'); ?>">
				<input type="hidden" name="idUsuario"  value="<?php echo $idUsuario; ?>">
				<input type="hidden" name="idEndereco" value="<?php echo $idEndereco; ?>">
				<input type="hidden" name="idComercio" value="<?php echo $idComercio; ?>">
			</div>
		</div>
	</form>
</div>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCwRKM3_Lamg5T3wTpWi9NhBcAYNZwdCdo&sensor=true&amp;libraries=places"></script>
<script type="text/javascript">
	$(document).ready(function(){

		$(".form-control, .disable-it").attr('disabled', 'disabled');
		$(".btn-hide").hide();
		$("#btn-editar").click(function(){
			$(".form-control, .disable-it").removeAttr('disabled');
			$(".btn-hide").show();
			$(this).hide();
		});
        
        function initialize() {
            var address = document.getElementById('endereco');
            var autocomplete = new google.maps.places.Autocomplete(address);

            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();
                var cidade = null;

                var info = place.address_components;
                $.each(info, function(key, value) {

                    if (value.types[0] == "neighborhood") {
                        var bairro = value.long_name;
                        document.getElementById('bairro').value = bairro;
                    }

                    if (value.types[0] == "street_number") {
                        var numero = value.long_name;
                        document.getElementById('numero').value = numero;
                    }

                    if (value.types[0] == "locality") {
                        cidade = value.long_name;
                        document.getElementById('cidade').value = cidade;
                    }
                })

                if (place.location != '') {
                    document.getElementById('cidade').value = cidade;
                    document.getElementById('latitude').value = place.geometry.location.lat();
                    document.getElementById('longitude').value = place.geometry.location.lng();
                }

            });

            $('#endereco').keypress(function(e) {
                if (e.which == 13) {
                    e.preventDefault();
                    google.maps.event.trigger(autocomplete, 'place_changed');
                    return false;
                }
            });


        }
        initialize();
	});
</script>
