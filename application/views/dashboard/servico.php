<div class="col-lg-10 col-md-10">
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong>
			<small>Serviços</small>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<span class="badge"><?php echo $lista['numrows']; ?></span> - Servico(s)
			<a href="<?php echo site_url('dashboard/Servico/cadastrar'); ?>" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> Serviços </a>
			<br clear="all">
		</div>
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Serviço</th>
					<th>Status</th>
					<th class="hidden-xs">DataHora</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
				for ($i=0; $i < count($lista['result']); $i++) { 
					?>
					<tr>
						<td><?php echo $lista['result'][$i]->idServico; ?></td>
						<td><?php echo $lista['result'][$i]->servico; ?></td>
						<td><?php echo $lista['result'][$i]->status; ?></td>
						<td><?php echo $lista['result'][$i]->dataHora; ?></td>
						<td class="text-right">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									Ação <span class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu">
									<li><a href="<?php echo site_url('dashboard/Servico/visualizar') .'/'. $lista['result'][$i]->idServico; ?>">Visualizar</a></li>
								</ul>
							</div>
						</td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
	</div>
</div>