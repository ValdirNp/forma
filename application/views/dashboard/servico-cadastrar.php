<?php
$nome      = (isset($servico['result'][0]->servico)   ? $servico['result'][0]->servico   : '');
$status    = (isset($servico['result'][0]->status)    ? $servico['result'][0]->status    : '');
$idServico = (isset($servico['result'][0]->idServico) ? $servico['result'][0]->idServico : NULL);
?>

<div class="col-lg-10 col-md-10">
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong>
			<small>Serviços</small>
		</div>
	</div>
	<h1>Cadastrar Serviço</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit expedita dicta doloremque, ea, repellendus voluptatem, harum neque minus quisquam hic perferendis commodi suscipit amet. Doloremque ut, alias animi. Animi, consequatur.</p>
	<br>
	<br>
	<div class="row">
		
		<form action="<?php echo site_url('dashboard/Servico/cadastrar'); ?>" method="post" enctype="multipart/form-data">
			<div class="col-md-12">
				<div class="form-group">
					<label for="servico">Serviço</label>
					<input type="text" name="servico" class="form-control" placeholder="Serviço" id="servico" value="<?php echo $nome; ?>">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label for="status">Status</label>
					<select name="status" class="form-control" id="status">
						<option value="">Selecione</option>
						<option value="On-Line" <?php echo (($status == 'On-Line')  ? 'selected' : ''); ?>>On-Line</option>
						<option value="Off-Line"<?php echo (($status == 'Off-Line') ? 'selected' : ''); ?>>Off-Line</option>
					</select>
				</div>
			</div>

			<!-- submit -->
			<div class="col-md-12">
				<div class="form-group">
					<br>
					<input type="submit" class="btn btn-primary" value="<?php echo (($idServico != '') ? 'Atualizar' : 'Cadastrar'); ?>">
					<?php if ($idServico): ?>
						<input type="hidden" name="idServico"  value="<?php echo $idServico; ?>">
					<?php endif ?>
				</div>
			</div>
		</form>
	</div>
</div>