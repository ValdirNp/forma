<div class="col-lg-10 col-md-10">
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong>
			<small>Avaliações</small>
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	<ul class="nav nav-tabs">
		<li role="presentation" class="active"><a href="#aprovados" data-toggle="tab">Aprovados</a></li>
		<li role="presentation"><a href="#reprovados" data-toggle="tab">Reprovados</a></li>
		<li role="presentation"><a href="#pendentes" data-toggle="tab">Pendentes</a></li>
	</ul>

	<div class="tab-content">
		<div class="tab-pane fade in active" id="aprovados">
			<?php foreach ($aprovadas as $aprovada): ?>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Avaliação - <?= $aprovada->usuario; ?></h3>
					</div>
					<div class="panel-body">
						<div class="dashboard-text">
							<ul>
								<li>Usuário: <strong><?= $aprovada->usuario; ?></strong></li>
								<li>Cliente: <strong><?= $aprovada->cliente; ?></strong></li>
								<li>Status: <strong class="text-success">Aprovado</strong></li>
							</ul>
							<div>
								<strong>Avaliação:</strong>
								<p><?= $aprovada->comentario; ?></p>
							</div>
							<div>
								<a href="<?= site_url('dashboard/Avaliacoes/mudar_status/Reprovado/'.$aprovada->id); ?>" class="btn btn-danger">Reprovar</a>
							</div>
						</div>
					</div>
				</div>		
			<?php endforeach ?>
		</div>

		<div class="tab-pane fade" id="reprovados">
			<?php foreach ($reprovadas as $reprovada): ?>				
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Avaliação - <?= $reprovada->usuario; ?></h3>
					</div>
					<div class="panel-body">
						<div class="dashboard-text">
							<ul>
								<li>Usuário: <strong><?= $reprovada->usuario; ?></strong></li>
								<li>Cliente: <strong><?= $reprovada->cliente; ?></strong></li>
								<li>Status: <strong class="text-danger">Reprovado</strong></li>
							</ul>
							<div>
								<strong>Avaliação:</strong>
								<p><?= $reprovada->comentario; ?></p>
							</div>
							<div>
								<a href="<?= site_url('dashboard/Avaliacoes/mudar_status/Aprovado/'.$reprovada->id); ?>" class="btn btn-success">Aprovar</a>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		</div>

		<div class="tab-pane fade" id="pendentes">
			<?php foreach ($pendentes as $pendente): ?>
				<div class="panel panel-default">
					<div class="panel-heading">
					<h3 class="panel-title"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Avaliação - <?= $pendente->usuario; ?></h3>
					</div>
					<div class="panel-body">
						<div class="dashboard-text">
							<ul>
							<li>Usuário: <strong><?= $pendente->usuario; ?></strong></li>
								<li>Cliente: <strong><?= $pendente->cliente; ?></strong></li>
								<li>Status: <strong class="text-warning">Não avaliado</strong></li>
							</ul>
							<div>
								<strong>Avaliação:</strong>
								<p><?= $pendente->comentario; ?></p>
							</div>
							<div>
								<a href="<?= site_url('dashboard/Avaliacoes/mudar_status/Aprovado/'.$pendente->id); ?>" class="btn btn-success">Aprovar</a>
								<a href="<?= site_url('dashboard/Avaliacoes/mudar_status/Reprovado/'.$pendente->id); ?>" class="btn btn-danger">Reprovar</a>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		</div>
	</div>
	<!-- PAGINATION -->
	<!-- <nav>
		<ul class="pagination">
			<li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
		</ul>
	</nav> -->
</div>