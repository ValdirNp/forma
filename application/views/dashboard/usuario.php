<?php $url = ((($this->session->userdata('tipo')) && ($this->session->userdata('tipo') == 'cliente')) ? 'cliente' : 'dashboard'); ?>

<div class="col-lg-10 col-md-10">
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong>
			<small>Usuários</small>
		</div>
	</div>
	<div class="row mb-20">
		<div class="col-lg-5">
			<form class="form-inline" id="busca_usuario" action="<?= site_url('dashboard/Usuario/buscar'); ?>" method="POST">
				<div class="form-group">
					<label>Pesquisar</label>
					<input type="text" class="form-control" name="search" id="search">
					<input type="submit" value="OK" class="btn btn-primary">
				</div>
			</form>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<span class="badge" id="badge"><?php echo $lista['numrows']; ?></span> Usuário(s)
			<a href="<?php echo site_url(''.$url.'/Usuario/cadastrar'); ?>" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> Usuários </a>
			<br clear="all">
		</div>
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Foto</th>
					<th>Nome</th>
					<th>Email</th>
					<th>Telefone</th>
					<th>Celular</th>
					<th class="hidden-xs">DataHora</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="tbody">
				<?php for ($i=0; $i < count($lista['result']); $i++) { ?>
				<tr>
					<td><?php echo $lista['result'][$i]->idUsuario; ?></td>
					<td><img src="<?php echo base_url('common/data') .'/'. $lista['result'][$i]->foto; ?>" style="width: 55px;"></td>
					<td><?php echo $lista['result'][$i]->nome; ?></td>
					<td><?php echo $lista['result'][$i]->email; ?></td>
					<td><?php echo $lista['result'][$i]->telefone; ?></td>
					<td><?php echo $lista['result'][$i]->celular; ?></td>
					<td><?php echo $lista['result'][$i]->dataHora; ?></td>
					<td><a href="<?php echo site_url(''.$url.'/Usuario/comentarios') .'/'. $lista['result'][$i]->idUsuario; ?>"><i class="fa fa-comment"></i></a></td>
					<td><a href="<?php echo site_url(''.$url.'/Usuario/carro') .'/'. $lista['result'][$i]->idUsuario; ?>"><span class="glyphicon glyphicon-wrench"></span></a></td>
					<td><a href="<?php echo site_url(''.$url.'/Usuario/visualizar') .'/'. $lista['result'][$i]->idUsuario; ?>"><span class="glyphicon glyphicon-eye-open"></span></a></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<a class="btn btn-default" href="<?= site_url(''.$url.'/Usuario/exportar'); ?>">Exportar XML</a>
</div>
<script>
	$('#busca_usuario').submit(function(event){
		event.preventDefault();
		var form = $(this);
		$.ajax({
			url: form[0].action,
			data: {
				search: $('#search').val()
			},
			type: 'POST',
			success: function(data){
				$('#tbody').html(data);
				$('#badge').hide();
			}
		});
	});
</script>