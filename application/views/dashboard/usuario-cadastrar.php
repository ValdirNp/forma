<?php
	// url
$url = ((($this->session->userdata('tipo')) && ($this->session->userdata('tipo') == 'cliente')) ? 'cliente' : 'dashboard');

	//id
$idUsuario  = (isset($usuario['result'][0]->idUsuario)   ? $usuario['result'][0]->idUsuario   : NULL);
$idEndereco = (isset($endereco['result'][0]->idEndereco) ? $endereco['result'][0]->idEndereco : '');
$idComercio = (isset($comercio['result'][0]->idComercio) ? $comercio['result'][0]->idComercio : '');

	// informações
$nome      = (isset($usuario['result'][0]->nome)      ? $usuario['result'][0]->nome     : '');
$sobrenome = (isset($usuario['result'][0]->sobrenome)      ? $usuario['result'][0]->sobrenome     : '');
$email     = (isset($usuario['result'][0]->email)     ? $usuario['result'][0]->email    : '');
$celular   = (isset($usuario['result'][0]->celular)   ? $usuario['result'][0]->celular  : '');
$telefone  = (isset($usuario['result'][0]->telefone)  ? $usuario['result'][0]->telefone : '');
$cpf       = (isset($usuario['result'][0]->CPF)       ? $usuario['result'][0]->CPF      : '');
$senha     = (isset($usuario['result'][0]->senha)     ? $usuario['result'][0]->senha    : '');
$foto      = (isset($usuario['result'][0]->foto)      ? $usuario['result'][0]->foto     : '');
$idStatus  = (isset($usuario['result'][0]->idStatus)  ? $usuario['result'][0]->idStatus  : '');

	// endereco
$cep         = (isset($endereco['result'][0]->cep) 		   ? $endereco['result'][0]->cep 		   : '');
$rua    	 = (isset($endereco['result'][0]->endereco)    ? $endereco['result'][0]->endereco      : '');
$numero      = (isset($endereco['result'][0]->numero) 	   ? $endereco['result'][0]->numero 	   : '');
$bairro      = (isset($endereco['result'][0]->bairro) 	   ? $endereco['result'][0]->bairro 	   : '');
$cidade      = (isset($endereco['result'][0]->cidade) 	   ? $endereco['result'][0]->cidade 	   : '');
$estado      = (isset($endereco['result'][0]->estado) 	   ? $endereco['result'][0]->estado 	   : '');
$complemento = (isset($endereco['result'][0]->complemento) ? $endereco['result'][0]->complemento   : '');
?>

<div class="col-lg-10 col-md-10">
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong>
			<small>Usuários</small>
		</div>
	</div>
	<h1>Cadastrar Usuário</h1>
	<br>
	<br>

	<form action="<?php echo site_url(''.$url.'/Usuario/cadastrar'); ?>" method="post" enctype="multipart/form-data">
		<div class="col-md-4">
			<div class="form-group">
				<label for="servico">Nome</label>
				<input type="text" name="nome" class="form-control" placeholder="Nome" id="nome" value="<?php echo $nome; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="servico">Sobrenome</label>
				<input type="text" name="sobrenome" class="form-control" placeholder="Sobrenome" id="sobrenome" value="<?php echo $sobrenome; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="servico">E-mail</label>
				<input type="text" name="email" class="form-control" placeholder="Email" id="email" value="<?php echo $email; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="servico">Celular</label>
				<input type="text" name="celular" class="form-control" placeholder="Celular" id="celular" value="<?php echo $celular; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="servico">Telefone</label>
				<input type="text" name="telefone" class="form-control" placeholder="Telefone" id="telefone" value="<?php echo $telefone; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="servico">CPF</label>
				<input type="text" name="cpf" class="form-control" placeholder="CPF" id="cpf" value="<?php echo $cpf; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="servico">Senha</label>
				<input type="password" name="senha" class="form-control" placeholder="Senha" id="senha" value="<?php echo $senha; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="servico">Foto</label>
				<input type="file" name="file" class="form-control" placeholder="Foto" id="file">

				<?php if ($foto != ''): ?>
					<input type="hidden" name="fotoDaEmpresaHidden" value="<?php echo $foto; ?>">
					<a href="<?php echo base_url('common/data/'. $foto .''); ?>" target="_blank" style="display: block; margin-top: 5px;">Visualizar foto</a>
				<?php endif ?>
			</div>
		</div>

		<!-- endereço -->
		<div class="col-md-12">
			<div class="form-group">
				<label for="cep">CEP</label>
				<input type="text" class="form-control" id="cep" placeholder="00000-000" name="cep" value="<?php echo $cep; ?>">
			</div>
		</div>
		<div class="col-md-10">
			<div class="form-group">
				<label for="endereco">Endereço</label>
				<input type="text" class="form-control" id="endereco" placeholder="Endereço" name="endereco" value="<?php echo $rua; ?>">
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<label for="numero">Número</label>
				<input type="text" class="form-control" id="numero" placeholder="Nº" name="numero" value="<?php echo $numero; ?>">
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label for="complemento">Complemento</label>
				<input type="text" class="form-control" id="complemento" placeholder="Complemento" name="complemento" value="<?php echo $complemento; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="bairro">Bairro</label>
				<input type="text" class="form-control" id="bairro" placeholder="Bairro" name="bairro" value="<?php echo $bairro; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="cidade">Cidade</label>
				<input type="text" class="form-control" id="cidade" placeholder="Cidade" name="cidade" value="<?php echo $cidade; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="estado">Estado</label>
				<input type="text" class="form-control" id="estado" placeholder="Estado" name="estado" value="<?php echo $estado; ?>">
			</div>
		</div>


		<div class="col-md-2">
			<div class="form-group">
				<label for="status">Status</label>
				<select name="status" class="form-control" id="status">
					<option value="">Selecione</option>
					<option value="On-Line" <?php echo (($idStatus == '1')  ? 'selected' : ''); ?>>On-Line</option>
					<option value="Off-Line"<?php echo (($idStatus == '2') ? 'selected' : ''); ?>>Off-Line</option>
				</select>
			</div>
		</div>

		<!-- submit -->
		<div class="col-md-12">
			<div class="form-group">
				<br>
				<input type="button" id="btn-editar" class="btn btn-success procurarCEP" value="Editar">
				<input type="submit" class="btn btn-primary btn-hide" value="<?php echo (($idUsuario != '') ? 'Atualizar' : 'Cadastrar'); ?>">
				<input type="hidden" name="idUsuario"  value="<?php echo $idUsuario; ?>">
				<input type="hidden" name="idEndereco" value="<?php echo $idEndereco; ?>">
				<input type="hidden" name="idComercio" value="<?php echo $idComercio; ?>">
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
		$(document).ready(function(){
				$(".form-control, .disable-it").attr('disabled', 'disabled');
				$(".btn-hide").hide();
				$("#btn-editar").click(function(){
					$(".form-control, .disable-it").removeAttr('disabled');
					$(".btn-hide").show();
					$(this).hide();
					$("#senha").attr('disabled', 'disabled');
				});
		});
</script>