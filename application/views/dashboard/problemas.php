<div class="col-lg-10 col-md-10">
    <div class="panel panel-default">
        <div class="panel-body agendamentos">
            <span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong>
            <small>Problemas reportados</small>
        </div>
    </div>

    <div class="clearfix"></div>

    <?php foreach ($problemas as $problema): ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Problema do usuário: <?= $problema->nome; ?></h3>
            </div>
            <div class="panel-body">
                <div class="dashboard-text">
                    <p>
                        <?= $problema->problema; ?>
                    </p>
                    <div>
                    <?php foreach ($comentarios as $comentario):
                        if($comentario->idProblema == $problema->idProblema){?>
                            <div class="panel panel-default" style="margin-top: 20px;">
                                <div class="panel-heading">
                                    <h3 class="panel-title"></span> Comentário de: <?= $comentario->nome; ?></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="dashboard-text">
                                        <p>
                                            <?= $comentario->comentario; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php endforeach; ?>

                        <form action="<?php echo site_url('dashboard/Problemas/comentar'); ?>" method="post" enctype="multipart/form-data">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea name="comentario" id="comentario" class="form-control" placeholder="Seu comentário" rows="5"></textarea>
                                </div>
                            </div>
                            <input type="hidden" name="idProblema" value="<?= $problema->idProblema ?>">
                            <input type="hidden" name="idUsuario" value="<?= $this->session->userdata('idUsuario') ?>">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary pull-right" value="Comentar">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div>

                    </div>
                </div>
            </div>
        </div>
    <?php endforeach ?>

    <!-- PAGINATION -->
    <!-- <nav>
        <ul class="pagination">
            <li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
        </ul>
    </nav> -->
</div>