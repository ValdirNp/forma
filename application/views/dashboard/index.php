<!DOCTYPE html>
<html lang="en">
<head>
	<title>Orbit - Ferramenta Mobile</title>
	<!-- meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- css -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url('common/css/mdp.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('common/css/dash.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('common/css/dashboard.css'); ?>">

	
	<!-- js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>

	<script type="text/javascript" src="<?php echo base_url('common/js/jquery-ui-1.11.1.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('common/js/jquery-2.1.1.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('common/js/jquery-ui-1.11.1.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('common/js/jquery-ui.multidatespicker.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('common/js/Chart.js'); ?>"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url('common/javascript/cliente.js'); ?>" ></script>	

</head>

<body>
	<div class="site">
		<div class="container">
			<!-- menu -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							<li>
								<a href="#">
									<span class="glyphicon glyphicon-user"></span> <?php echo $this->session->userdata('nome'); ?> 
								</a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
									<span class="glyphicon glyphicon-menu-hamburger"></span>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="<?php echo site_url('dashboard/Login/logout'); ?>">Logout</a></li>
									<li class="divider"></li>
									<li><a href="#">Editar perfil</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- menu fim -->

			<!-- flashData -->
			<div class="form-group">
				<?php
				$flashdata = $this->session->flashdata('flashdata');
				if ($flashdata) {
					?>
					<div class="alert <?php echo $flashdata['type']; ?> alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<?php echo $flashdata['msg']; ?>
					</div>
					<?php
				}
				?>
			</div>
			<!-- flashData fim -->
			
			<!-- conteudo -->
			<div class="col-md-12">
				<div class="row">
					<div class="row">
						<div class="col-lg-2 col-md-2">
							<img src="<?= base_url('common/image/logo.png'); ?>" alt="Logo" class="lateral">
							<div class="list-group">
								<a href="<?php echo site_url('dashboard/Home'); ?>" class="list-group-item active"><span class="glyphicon glyphicon-th-large"></span> Dashboard</a>
								<button data-toggle="collapse" data-target="#clients" class="list-group-item parent"><span class="glyphicon glyphicon-wrench"></span> Clientes <span class="glyphicon glyphicon-chevron-right pull-right"></span>
								</button>
								<div id="clients" class="sublinks floating collapse">
									<a href="<?= site_url('dashboard/Cliente'); ?>" class="list-group-item small"><span class="glyphicon glyphicon-wrench"></span> Visualizar</a>
									<a href="<?= site_url('dashboard/Cliente/cadastrar'); ?>" class="list-group-item small"><span class="glyphicon glyphicon-plus"></span> Cadastrar</a>
								</div>
								<button data-toggle="collapse" data-target="#users" class="list-group-item parent"><span class="glyphicon glyphicon-user"></span> Usuários <span class="glyphicon glyphicon-chevron-right pull-right"></span>
								</button>
								<div id="users" class="sublinks floating collapse">
									<a href="<?= site_url('dashboard/Usuario'); ?>" class="list-group-item small"><span class="glyphicon glyphicon-user"></span> Visualizar</a>
									<a href="<?= site_url('dashboard/Usuario/cadastrar'); ?>" class="list-group-item small"><span class="glyphicon glyphicon-plus"></span> Cadastrar</a>
								</div>
								<button data-toggle="collapse" data-target="#agend" class="list-group-item parent"><span class="glyphicon glyphicon-calendar"></span> Agendamentos <span class="glyphicon glyphicon-chevron-right pull-right"></span>
								</button>
								<div id="agend" class="sublinks floating collapse">
									<a href="<?= site_url('dashboard/Agendamento'); ?>" class="list-group-item small"><span class="glyphicon glyphicon-calendar"></span> Visualizar</a>
									<a href="<?= site_url('dashboard/Agendamento/cadastrar'); ?>" class="list-group-item small"><span class="glyphicon glyphicon-plus"></span> Cadastrar</a>
								</div>
								<a href="<?php echo site_url('dashboard/Avaliacoes'); ?>" class="list-group-item"><span class="glyphicon glyphicon-ok"></span> Avaliações</a>
								<a href="<?php echo site_url('dashboard/Problemas'); ?>" class="list-group-item"><span class="glyphicon glyphicon-warning-sign"></span> Reportar problemas</a>
								<a href="<?php echo site_url('dashboard/Notificacoes'); ?>" class="list-group-item"><span class="glyphicon glyphicon-flag"></span> Notificações</a>
								<a href="#" class="list-group-item"><span class="glyphicon glyphicon-credit-card"></span> Cartão Fidelidade</a>
								<a href="<?= site_url('dashboard/Relatorios'); ?>" class="list-group-item"><span class="glyphicon glyphicon-stats"></span> Relatórios</a>
								<button data-toggle="collapse" data-target="#config" class="list-group-item parent"><span class="glyphicon glyphicon-cog"></span> Configurações <span class="glyphicon glyphicon-chevron-right pull-right"></span>
								</button>
								<div id="config" class="sublinks floating collapse">
									<a href="<?= site_url('dashboard/Configuracoes'); ?>" class="list-group-item small"><span class="glyphicon glyphicon-cog"></span> Configurações</a>
									<a href="<?= site_url('dashboard/Servico'); ?>" class="list-group-item small"><span class="glyphicon glyphicon-wrench"></span> Serviços</a>
									<a href="<?= site_url('dashboard/Cliente'); ?>" class="list-group-item small"><span class="glyphicon glyphicon-wrench"></span> Clientes</a>
									<a href="<?= site_url('dashboard/Usuario'); ?>" class="list-group-item small"><span class="glyphicon glyphicon-user"></span> Usuários</a>
								</div>
							</div>
						</div>
						<?php echo $contents; ?>
					</div>
				</div>
			</div>
			<!-- conteudo fim -->
		</div>
	</div>
</body>
<script>
	$('.parent').click(function(event) {
		console.log($(this))
		event.preventDefault();
		target = $(this).data('target');
		if($(target).css('display') == 'block') {
			$(target).fadeOut();
		} else {
			$('.sublinks').fadeOut();
			$(target).fadeIn();
		}
	});
</script>
</html>

