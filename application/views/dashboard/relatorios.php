<div class="col-lg-10 col-md-10">
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong>
			<small>Relatórios</small>
		</div>
	</div>
	<!-- GRAFICOS -->
	<div class="row">
		<!-- AGENDAMENTO POR PERIODO -->
		<div class="col-lg-6 mb-30">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-calendar"></span> Agendamentos por período - Últimos 7 dias</div>
				<div class="panel-body">
					<canvas class="dashbord-box-content" id="chart-agendamento" style="width:426px; height:200px"></canvas>
				</div>
			</div>
		</div>

		<!-- CADASTRO DE USUARIOS -->
		<div class="col-lg-6 mb-30">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-calendar"></span> Cadastro de usuários - Últimos 7 dias</div>
				<div class="panel-body">
					<canvas class="dashbord-box-content" id="chart-usuarios" style="width:426px; height:200px"></canvas>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<!-- CADASTRO DE CLIENTES -->
		<div class="col-lg-6 mb-30">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-user"></span> Cadastro de clientes - Últimos 7 dias</div>
				<div class="panel-body">
					<canvas class="dashbord-box-content" id="chart-clientes" style="width:426px; height:200px"></canvas>
				</div>
			</div>
		</div>

		<!-- CADASTRO DE CADASTRO DE AGENDAMENTO -->
		<div class="col-lg-6 mb-30">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-user"></span> Cadastro de agendamento - Últimos 7 dias</div>
				<div class="panel-body">
					<canvas class="dashbord-box-content" id="chart-cadastro" style="width:426px; height:200px"></canvas>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 mb-30">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-user"></span> Quantidade de usuários cadastrados por clientes - Últimos 7 dias</div>
				<div class="panel-body">
					<canvas class="" id="chart-qtd" style="width:426px; height:200px"></canvas>
				</div>
			</div>
		</div>
	</div>

	<!-- HISTORICO AGENDAMENTO -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-calendar"></span> Últimos agendamentos<a href="<?= site_url('dashboard/Agendamento'); ?>">ver lista completa</a></div>
				<table class="table">
					<thead>
						<tr>
							<th>Ref</th>
							<th>Nome</th>
							<th>Cliente</th>
							<th>Data</th>
							<th>Serviço</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<?php $limit = count($lista); ?>
							<?php if ($limit > 5) $limit = 5; ?>
							<?php for ($i=0; $i < $limit; $i++): ?> 
								<tr>
									<td><?= $lista[$i]['id']; ?></td>
									<td><?= $lista[$i]['nome']; ?></td>
									<td><?= $lista[$i]['comercio']; ?></td>
									<td><?= $lista[$i]['data']; ?> - <?= $lista[$i]['horario']; ?></td>
									<td><?= $lista[$i]['servico']; ?></td>
									<td class="<?= $lista[$i]['class']; ?>"><?= $lista[$i]['status']; ?></td>
								</tr>
							<?php endfor ?>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- HISTORICO AGENDAMENTO -->
	<!-- <div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-calendar"></span> Últimas chamadas de emergência</div>
				<table class="table">
					<thead>
						<tr>
							<th>Ref</th>
							<th>User</th>
							<th>Cliente</th>
							<th>Data</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td scope="row">0001</td>
							<td>Thiago Jordano</td>
							<td>QualiCenter Auto</td>
							<td>20/06/2015 - 10:11</td>
							<td><strong class="text-success">Atendido</strong></td>
						</tr>
						<tr>
							<td scope="row">0001</td>
							<td>Thiago Jordano</td>
							<td>QualiCenter Auto</td>
							<td>20/06/2015 - 15:15</td>
							<td><strong class="text-danger">Não atendido</strong></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div> -->

	<!-- USUARIOS -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><span class="glyphicon glyphicon-user"></span> Últimos usuários cadastrados<a href="<?= site_url('dashboard/Usuario'); ?>">ver lista completa</a></div>
				<table class="table">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Email</th>
							<th>Telefone</th>
							<th>Data</th>
						</tr>
					</thead>
					<tbody>
						<?php $aux = 0; ?>
						<?php foreach ($usuarios['result'] as $usuario): ?>
							<?php if ($aux < 5): ?>
								<tr>
									<td><?= $usuario->nome; ?></td>
									<td><?= $usuario->email; ?></td>
									<td><?= $usuario->telefone; ?></td>
									<td><?= $usuario->dataHora; ?></td>
								</tr>
								<?php $aux++; ?>
							<?php endif ?>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
<?php 
		  
		     
		      
?>
$(document).ready(function(){
     var chart_qtd = document.getElementById('chart-qtd').getContext("2d");
     var chart_agendamento = document.getElementById('chart-agendamento').getContext("2d");
      var chart_usuarios = document.getElementById('chart-usuarios').getContext("2d");
      var chart_cadastro = document.getElementById('chart-cadastro').getContext("2d");
      var chart_clientes = document.getElementById('chart-clientes').getContext("2d");
     
      var data_qtd = <?= $barChart ?>;
      var data_agend = <?= $agendamentoPeriodo ?>;
      var data_user = <?= $usuarioschart ?>;
      var data_cliente = <?= $clienteschart ?>;
      var data_cadastro = <?= $agendamentoChart ?>;
    

	 var barOptions = {										    
	    scaleBeginAtZero : true,					//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
	    scaleShowGridLines : true,					//Boolean - Whether grid lines are shown across the chart
	    scaleGridLineColor : "rgba(0,0,0,.05)",		//String - Colour of the grid lines
	    scaleGridLineWidth : 1,						//Number - Width of the grid lines
	    scaleShowHorizontalLines: true,				//Boolean - Whether to show horizontal lines (except X axis)
	    scaleShowVerticalLines: true,				//Boolean - Whether to show vertical lines (except Y axis)
	    barShowStroke : true,						//Boolean - If there is a stroke on each bar
	    barStrokeWidth : 2,							//Number - Pixel width of the bar stroke
	    barValueSpacing : 5,    					//Number - Spacing between each of the X value sets
	    barDatasetSpacing : 1,  					//Number - Spacing between data sets within X values
    	legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"//String - A legend template

	 };
     var lineOptions = {
	    scaleShowGridLines : true,	//Boolean - Whether grid lines are shown across the chart
	    scaleGridLineColor : "rgba(0,0,0,.05)",	//String - Colour of the grid lines
	    scaleGridLineWidth : 1,	//Number - Width of the grid lines
	    scaleShowHorizontalLines: true,	//Boolean - Whether to show horizontal lines (except X axis)
	    scaleShowVerticalLines: true,	//Boolean - Whether to show vertical lines (except Y axis)
	    bezierCurve : false,	//Boolean - Whether the line is curved between points
	    bezierCurveTension : 0.4,	//Number - Tension of the bezier curve between points
	    pointDot : true,	//Boolean - Whether to show a dot for each point
	    pointDotRadius : 4,	//Number - Radius of each point dot in pixels
	    pointDotStrokeWidth : 1,	//Number - Pixel width of point dot stroke
	    pointHitDetectionRadius : 20,	//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
	    datasetStroke : true,	//Boolean - Whether to show a stroke for datasets
	    datasetStrokeWidth : 2,	//Number - Pixel width of dataset stroke
	    datasetFill : true,	//Boolean - Whether to fill the dataset with a colour
	    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"	//String - A legend templ

	};

     new Chart(chart_qtd).Bar(data_qtd, barOptions);
      new Chart(chart_agendamento).Line(data_agend, lineOptions); 
       new Chart(chart_usuarios).Line(data_user, lineOptions); 
      new Chart(chart_cadastro).Line(data_cadastro, lineOptions); 
       new Chart(chart_clientes).Line(data_cliente, lineOptions); 
 });
	
	
</script>