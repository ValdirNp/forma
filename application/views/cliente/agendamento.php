<div class="col-lg-10">
	<div class="panel panel-default">
		<div class="panel-body agendamentos">
			<span class="glyphicon glyphicon-th-large"></span> <strong>Dashboard</strong>
			<small>Agendamentos</small>
		</div>
	</div>
	
	<div class="panel panel-primary" id="canvas_calendar">
		<div class="panel-heading">
			<span class="glyphicon glyphicon-calendar"></span> <strong id="title-cal">Calendário - <?= gmdate('m/Y'); ?></strong>
			<a href="" class="change-month pull-right" data-do="next">Próximo<span class="glyphicon glyphicon-chevron-right"></span></a>
			<a href="" class="change-month pull-right" data-do="prev"><span class="glyphicon glyphicon-chevron-left"></span>Anterior</a>
		</div>
		<div class="panel-body" id="calendar_agendamento">
		</div>
	</div>

	<div class="modal fade" id="detalheAgend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Informações</h4>
				</div>
				<div class="modal-body">
					<p id="agend-usuario"></p>
					<p id="agend-servico"></p>
					<p id="agend-data"></p>
					<p id="agend-horario"></p>
					<p id="agend-status"></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					<button type="button" class="btn btn-danger agend-action" id="cancel-agend" data-id="" data-status="Cancelado">Cancelar atendimento</button>
					<button type="button" class="btn btn-primary agend-action" id="confirm-agend" data-id="" data-status="Confirmado">Confirmar atendimento</button>
					<button type="button" class="btn btn-success agend-action" id="finish-agend" data-id="" data-status="Concluido">Atendimento realizado</button>
					<button type="button" class="btn btn-default" id="add-info" data-id="">Adicionar informações</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="addInfo" tabindex="-1" role="dialog" aria-labelledby="infoModal">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="infoModal">Informações adicionais</h4>
				</div>
				<div class="modal-body">
					<form id="formAddInfo" action="<?php echo site_url('cliente/Agendamento/add_info'); ?>" method="POST">
						<input type="hidden" name="idAgend" id="idAgend">
						<div class="form-group">
							<label for="valorPeca">Valor das Peças: </label>
							<input class="form-control" type="text" name="valorPeca" id="valorPeca">
						</div>
						<div class="form-group">
							<label for="valorServico">Valor do Serviço: </label>
							<input class="form-control" type="text" name="valorServico" id="valorServico">
						</div>
						<div class="form-group">
							<label for="obs">Observações: </label>
							<textarea name="obs" id="obs" class="form-control"></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					<button type="button" class="btn btn-success" id="saveInfo" data-id="">Salvar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(event){
		var month = new Date().getMonth() + 1;
		var year = new Date().getFullYear();
		var id = $(this).val();
		$.ajax({
			url : '<?= site_url("cliente/Agendamento/draw_calendar"); ?>',
			type : 'POST',
			data : {
				id: id,
				mes: month,
				ano: year
			},
			success : function(data){
				$('#canvas_calendar').show();
				$('#calendar_agendamento').html(data);
				listenSelect();
			}
		});
	});

	$('.change-month').click(function(event){
		event.preventDefault();
		var id = $("#client_list option:selected").val();
		var year = $('#calendario').data('year');
		var month = $('#calendario').data('month');
		if ($(this).data('do') == 'next') {
			month++;
			if (month == 13) {
				month = 1;
				year++;
			};
		} else {
			month--;
			if (month == 0) {
				month = 12;
				year--;
			};
		}
		$.ajax({
			url : '<?= site_url("cliente/Agendamento/draw_calendar"); ?>',
			type : 'POST',
			data : {
				id: id,
				mes: month,
				ano: year
			},
			success : function(data){
				$('#canvas_calendar').show();
				$('#calendar_agendamento').html(data);
				listenSelect();
				$('#title-cal').html('Calendário - ' + month + '/' + year);
			}
		});
	});

	function listenSelect (){
		$('.busy').change(function(){
			if ($(this).val() != 0) {
				$.ajax({
					url: '<?= site_url("cliente/Agendamento/get_detalhes"); ?>',
					type: 'POST',
					data: {
						id: $(this).val()
					},
					success: function(data){
						var dados = JSON.parse(data);
						$('.agend-action').attr('data-id', dados['id']);
						$('#agend-usuario').html('<b>Usuário: </b>' + dados['nome']);
						$('#agend-servico').html('<b>Serviço: </b>' + dados['servico']);
						$('#agend-data').html('<b>Data: </b>' + dados['data']);
						$('#agend-horario').html('<b>Horário: </b>' + dados['hora']);
						$('#agend-status').html('<b>Status: </b>' + dados['status']);
						$('#obs').html(dados['obs']);
						$('#valorPeca').val(dados['valorPeca']);
						$('#valorServico').val(dados['valorServico']);
						$('#idAgend').val(dados['id']);
						if (dados['status'] != 'Concluido') {
							$('#add-info').hide();
						};
						if (dados['status'] == 'Concluido') {
							$('#cancel-agend').hide();
							$('#confirm-agend').hide();
							$('#finish-agend').hide();
						};
						if (dados['status'] == 'Confirmado') {
							$('#cancel-agend').hide();
							$('#finish-agend').hide();
						};
						$('#detalheAgend').modal();
					}
				});
			};
		});
		$('[data-dismiss="modal"]').click(function(){
			$('.busy').val('0');
		});
	};

	$('.agend-action').click(function(event){
		event.preventDefault();
		$.ajax({
			url: '<?= site_url("cliente/Agendamento/atualizar_status"); ?>',
			type: 'POST',
			data: {
				id: $(this).data('id'),
				status: $(this).data('status')
			},
			success: function(){
				location.reload();
			}
		});
	});

	$('#add-info').click(function(event){
		var id = $(this).data('id');
		$('#detalheAgend').modal('hide');
		$('.busy').val('0');
		$('#id').val(id);
		$('#addInfo').modal();
	});

	$('#saveInfo').click(function(event){
		event.preventDefault();
		$.ajax({
			url: '<?= site_url("cliente/Agendamento/atualizar_info"); ?>',
			type: 'POST',
			data: {
				id: $('#idAgend').val(),
				valorPeca: $('#valorPeca').val(),
				valorServico: $('#valorServico').val(),
				obs: $('#obs').val()
			},
			success: function(){
				$('#addInfo').modal('hide');
				$('.busy').val('0');
			}
		});
	});

</script>