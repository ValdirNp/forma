<br clear="all">
<br clear="all">

<form action="<?php echo site_url('cliente/Servico/cadastrar'); ?>" method="post">
	<div class="col-md-8 col-md-offset-2">
		<div class="col-md-5">
			<div class="form-group">
				<label for="idComercio">Empresa</label>
				<select name="idComercio" class="form-control" id="idComercio">
					<?php
						for ($i=0; $i < count($dropdownDeComercio['result']); $i++) { 
							echo '<option value="'. $dropdownDeComercio['result'][$i]->idComercio .'">'. $dropdownDeComercio['result'][$i]->razao .'</option>';
						}
					?>
				</select>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<label for="idServico">Serviço</label>
				<select name="idServico" class="form-control" id="idServico">
					<option value="">Selecione</option>
					<?php
						for ($i=0; $i < count($dropdownDeServico['result']); $i++) { 
							echo '<option value="'. $dropdownDeServico['result'][$i]->idServico .'">'. $dropdownDeServico['result'][$i]->servico .'</option>';
						}
					?>
				</select>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<label style="visibility: hidden;">Serviço</label>
				<input type="submit" class="btn btn-primary" value="Cadastrar">
			</div>
		</div>
	</div>
</form>

<br clear="all">
<br clear="all">
<br clear="all">

<!-- success -->
<div class="panel panel-default">
	<div class="panel-heading">
		<span class="badge"><?php echo $listaDeServico['numrows']; ?></span> - Serviço(s)
	</div>
	<table class="table">
		<thead>
			<tr>
				<th>#</th>
				<th>Empresa</th>
				<th>Serviço</th>
				<th class="hidden-xs">DataHora</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				for ($i=0; $i < count($listaDeServico['result']); $i++) { 
					?>
						<tr>
							<td><?php echo $listaDeServico['result'][$i]->id; ?></td>
							<td><?php echo $listaDeServico['result'][$i]->razao; ?></td>
							<td><?php echo $listaDeServico['result'][$i]->servico; ?></td>
							<td><?php echo $listaDeServico['result'][$i]->dataHora; ?></td>
							<td class="text-right">
								<a href="<?php echo site_url('cliente/Servico/excluir/'. $listaDeServico['result'][$i]->id .''); ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
							</td>
						</tr>
					<?php
				}
			?>
		</tbody>
	</table>
</div>