<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

		$this->load->model(array(
			'Musuario',
			'Magendamento',
			'Mavaliacoes',
			'Mservico',
			'Mcomercio'
		));

		if ($this->Musuario->verificarNivel($this->session->userdata('idUsuario')) != 'Cliente') {
			$this->session->sess_destroy();
			redirect('dashboard/Login');
		}
	}

	public function index()
	{
		$agendamentoPeriodo = $this->get_line_charts($this->Magendamento->get_agendamento_periodo_charts());
		$usersPeriodo = $this->get_line_charts($this->Musuario->get_usuario_charts());
		$agendamento = array();
		$ag = $this->Magendamento->lista_agendamentos();				

		for ($i=0; $i < count($ag['result']); $i++) {
			$cm = $this->Mcomercio->lista_cliente($ag['result'][$i]->idComServ);
			$se = $this->Mservico->lista_cliente($ag['result'][$i]->idComServ);
			$us = $this->Musuario->lista_cliente(array('where' => array('nivel' => 'Usuario', 'idStatus' => 1, 'idUsuario' => $ag['result'][$i]->idUsuario)));

			switch ($ag['result'][$i]->status) {
				case 'Confirmado':
				case 'Concluido':
					$class = 'confirmed';
					break;
				case 'Pendente':
					$class = 'pending';
					break;
				case 'Cancelado':
					$class = 'canceled';
					break;
			}
						
			$agendamento[$i] = array(
				'id' => $ag['result'][$i]->id,
				'nome' => $ag['result'][$i]->nome . ' ' . $ag['result'][$i]->sobrenome,
				'status' => $ag['result'][$i]->status,
				'class' => $class,
				'data' => implode('/', array_reverse(explode('-', $ag['result'][$i]->data))),
				'horario' => $ag['result'][$i]->hora,
				'servico' => (isset($se['result'][0]->servico) ? $se['result'][0]->servico : ''),
				'comercio' => (isset($cm['result'][0]->razao) ? $cm['result'][0]->razao : ''),
				'usuario' => (isset($us['result'][0]->nome) ? $us['result'][0]->nome : ''),
				'dataHora' => $ag['result'][$i]->dataHora

			);
		}

		$this->template->load('cliente/index', 'cliente/home', array(
			'lista' => (($agendamento) ? $agendamento : array()),
			'agendamentos' => $this->Magendamento->lista(array()),
			'avaliacoes' => $this->Mavaliacoes->lista(array()),
			'usuarios' => $this->Musuario->lista_usuarios(),
			'agendamentoPeriodo' => $agendamentoPeriodo,
			'usersPeriodo' => $usersPeriodo
		));	
	}

	public function get_line_charts($array){

		$labels = array(
			date('d/m',mktime(0,0,0,date('m'), (date('d')-6), date('Y'))),
			date('d/m',mktime(0,0,0,date('m'), (date('d')-5), date('Y'))),
			date('d/m',mktime(0,0,0,date('m'), (date('d')-4), date('Y'))),
			date('d/m',mktime(0,0,0,date('m'), (date('d')-3), date('Y'))),
			date('d/m',mktime(0,0,0,date('m'), (date('d')-2), date('Y'))),
			date('d/m',mktime(0,0,0,date('m'), (date('d')-1), date('Y'))),
			date('d/m',mktime(0,0,0,date('m'), date('d'), date('Y'))),
		);
		foreach ($array as $a) {
			$index = array_search(date('d/m', strtotime($a->data)), $labels); 
			if($index !== false){
				$qtds[$index] = $a->qtd;
			}
		}

		//esse for preenche o resto dos indices que ficaram null com 0, porque não houveram agendamentos pra eles
		for($i=0;$i<7;$i++){
			if(!isset($qtds[$i])){
				$qtds[$i] = 0;
			}
		}

		//ordena por indice
		ksort($qtds);

		$datasets = array(
			array(
				"label" => "",
				"fillColor" => "rgba(151,187,205,0",
				"strokeColor" => "rgba(151,187,205,0.5",
				"pointColor" => "rgba(151,187,205,0.5",
				"pointStrokeColor" => "#fff",
				"pointHighlightFill" => "#fff",
				"pointHighlightStroke" => "rgba(151,187,205,0.5",
				"data" => $qtds
			)
		);

		$data_chart = array(
			"labels" => $labels,
			"datasets" =>$datasets
		);


		$Chart = json_encode($data_chart);

		return $Chart;

	}
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */