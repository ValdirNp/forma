<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

		$this->load->model(array(
			'Muc',
			'Mcarro',
			'Musuario',
			'Mcomercio',
			'Mendereco'
		));

		if ($this->Musuario->verificarNivel($this->session->userdata('idUsuario')) != 'Cliente') {
			$this->session->sess_destroy();
			redirect('dashboard/Login');
		} else {
			$this->session->set_userdata('tipo', 'cliente');
		}

		// initialize
		$this->upload->initialize(array(
			'upload_path' => 'common/data',
			'encrypt_name' => TRUE,
			'remove_spaces' => TRUE,
			'allowed_types' => 'jpg|jpeg'
		));
	}

	public function index()
	{
		$listaDeUsuario = $this->Musuario->lista(array(
			'where' => array('usuario.nivel' => 'Usuario')
		));

		$this->template->load('cliente/index', 'dashboard/usuario', array(
			'lista' => $listaDeUsuario
		));
	}

	public function cadastrar()
	{
		if ($this->input->post()) {
			if ((isset($_FILES['file'])) && ($_FILES['file']['size'] > 0)) {
				if (!$this->upload->do_upload('file')){
	                echo $this->upload->display_errors();
	                exit();
	            }

	            $upload = $this->upload->data();
	            $foto = $upload['file_name'];
	        } else {
	        	$foto = ((($this->input->post('fotoDaEmpresaHidden'))) ? $this->input->post('fotoDaEmpresaHidden') : NULL);
	        }

	        $usuario = $this->Musuario->cadastrar(array(
				'idUsuario' => (($this->input->post('idUsuario')) ? $this->input->post('idUsuario') : NULL),
				'nome' 	    => $this->input->post('nome'),
				'sobrenome' => $this->input->post('sobrenome'),
				'nivel'     => 'Usuario',
				'email'     => $this->input->post('email'),
				'idStatus'  => (($this->input->post('status') == 'On-Line') ? 1 : 2),
				'telefone'  => $this->input->post('telefone'),
				'celular'   => $this->input->post('celular'),
				'foto' 	    => $foto,
				'senha'     => substr(md5(uniqid(rand(), true)), 0, 8),
				'cpf' 	   => $this->input->post('cpf')
			));

	        if ($usuario['status']) {
				$comercio = $this->Mcomercio->cadastrar(array(
					'IdUsuario'       => $usuario['queryId'],
					'idComercio'      => (($this->input->post('idComercio')) ? $this->input->post('idComercio') : NULL),
				));

				
			}

			if ($comercio['status']) {
				$endereco = $this->Mendereco->cadastrar(array(
					'idEndereco'  => (($this->input->post('idEndereco')) ? $this->input->post('idEndereco') : NULL),
					'idComercio'  => $comercio['queryId'],
					'cep'	      => $this->input->post('cep'),
					'endereco' 	  => $this->input->post('endereco'),
					'numero' 	  => $this->input->post('numero'),
					'complemento' => $this->input->post('complemento'),
					'bairro' 	  => $this->input->post('bairro'),
					'cidade' 	  => $this->input->post('cidade'),
					'estado' 	  => $this->input->post('estado')
				));
			}

			$this->flashData($usuario['status']);
			redirect('cliente/Usuario');
		}

		$this->template->load('cliente/index', 'dashboard/usuario-cadastrar', array());
	}

	public function carro()
	{
		if ($this->input->post()) {
			$carro = $this->Mcarro->cadastrar($this->input->post());
			$this->flashData($carro['status']);
			redirect('cliente/Usuario/carro/'. $this->input->post('idUsuario') .'');
		}

		$this->template->load('cliente/index', 'dashboard/usuario-carro-cadastrar', array(
			'lista' => $this->Mcarro->lista($this->uri->segment(4)),
			'marcas' => $this->Mcarro->get_marcas()
		));
	}

	public function carroDelete()
	{
		$carro = $this->Mcarro->delete($this->input->get('carro'));
		$this->flashData($carro['status']);
		redirect('dashboard/Usuario/carro/'. $this->input->get('usuario') .'');
	}

	public function comercio()
	{
		if ($this->input->post()) {
			$comercio = $this->Muc->cadastrar($this->input->post());
			$this->flashData($comercio['status']);
			redirect('cliente/Usuario/comercio/'. $this->input->post('idUsuario') .'');
		}

		$comercio = $this->Mcomercio->lista(array(
			'where' => array('IdUsuario' => $this->session->userdata('idUsuario'))
		));

		$lista = $this->Muc->lista(array(
			'where' => array('usuario_comercio.idUsuario' => $this->uri->segment(4))
		));

		$this->template->load('cliente/index', 'dashboard/usuario-comercio', array(
			'comercio' => $comercio,
			'lista' => $lista
		));
	}

	public function comercioDelete()
	{
		$comercio = $this->Muc->delete($this->input->get('comerdio'));
		$this->flashData($comercio['status']);
		redirect('cliente/Usuario/comercio/'. $this->input->get('usuario') .'');
	}

	public function visualizar($id)
	{
		$usuario = $this->Musuario->lista(array(
			'where' => array('idUsuario' => $id)
		));

		$comercio = $this->Mcomercio->lista(array(
			'where' => array('idUsuario' => $id)
		));

		$endereco = $this->Mendereco->lista(array(
			'where' => array('idComercio' => $comercio['result'][0]->idComercio)
		));

		$this->template->load('cliente/index', 'dashboard/usuario-cadastrar', array(
			'usuario' => $usuario,
			'comercio' => $comercio,
			'endereco' => $endereco
		));
	}

	// default

	public function flashData($status)
	{
		$this->session->set_flashdata('flashdata', array(
			'msg'  => (($status) ? 'Operação efetuada com sucesso!' : 'Erro ao efetuar operaçãdo.'),
			'type' => (($status) ? 'alert-success' : 'alert-warning')
		));
	}
	
	public function exportar()
	{
		$this->load->dbutil();
	    $sql = "select * from usuario where nivel = 'Usuario'";
	    $query = $this->db->query($sql);
	    $config = array (
	        'root'    => 'root',
	        'element' => 'element',
	        'newline' => "\n",
	        'tab'     => "\t"
	    );
	 //    $filename = gmdate("d-m-Y_H-i", time());
  //   	header('Content-type: text/xml');
  //   	header("Content-disposition: xml" . gmdate("Y-m-d") . ".xml");
  //       header("Content-disposition: filename=" . $filename . ".xml");
		// echo $this->dbutil->xml_from_result($query, $config);
		// exit;
		$xml = $this->dbutil->xml_from_result($query, $config);
		$this->load->helper('file');
		$filename = 'export' . gmdate("d-m-Y_H-i") . '.xml';
		$path = 'common/xml/';
		write_file($path . $filename, $xml);
		// Optionally redirect to the file you (hopefully) just created
		// redirect($file_name);

		$this->load->helper('download');
		$data = file_get_contents($path . $filename); // Read the file's contents
		$name = $filename;

		force_download($name, $data);
	}
}

/* End of file Usuario.php */
/* Location: ./application/controllers/dashboard/Usuario.php */