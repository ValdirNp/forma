<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servico extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

		$this->load->model(array(
			'Mcs',
			'Musuario',
			'Mservico',
			'Mcomercio'
		));

		if ($this->Musuario->verificarNivel($this->session->userdata('idUsuario')) != 'Cliente') {
			$this->session->sess_destroy();
			redirect('dashboard/Login');
		}
	}

	public function index()
	{
		// meus dados
		$meusDados = $this->Mcomercio->meuComercio();
		
		$this->template->load('cliente/index', 'cliente/servico', array(
			'dropdownDeServico' => $this->Mservico->lista(array('where' => array('status' => 'On-Line'))),
			'dropdownDeComercio' => $meusDados,
			'listaDeServico' => $this->Mcs->lista($meusDados['result'][0]->idComercio)
		));
	}

	public function cadastrar()
	{
		if ($this->input->post()) {
			$query = $this->Mcs->cadastrar($this->input->post());
			$this->flashData($query['status']);
		}

		redirect('cliente/Servico');
	}

	public function excluir($id)
	{
		$query = $this->Mcs->excluir($id);
		$this->flashData($query['status']);
		redirect('cliente/Servico');
	}

	public function flashData($status)
	{
		$this->session->set_flashdata('flashdata', array(
			'msg'  => (($status) ? 'Operação efetuada com sucesso!' : 'Erro ao efetuar operaçãdo.'),
			'type' => (($status) ? 'alert-success' : 'alert-warning')
		));
	}

}

/* End of file Servico.php */
/* Location: ./application/controllers/cliente/Servico.php */