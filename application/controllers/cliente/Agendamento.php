<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agendamento extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

		$this->load->model(array(
			'Mcs',
			'Musuario',
			'Mservico',
			'Mcomercio',
			'Magendamento'
		));

		if ($this->Musuario->verificarNivel($this->session->userdata('idUsuario')) != 'Cliente') {
			$this->session->sess_destroy();
			redirect('dashboard/Login');
		}
	}

	public function index()
	{
		$clientes = $this->Musuario->lista(array(
			'tab' => 'usuario',
			'where' => array('usuario.nivel' => 'Cliente')
			));

		$this->template->load('cliente/index', 'cliente/agendamento', array(
			'clientes' => $clientes
		));
	}

	public function draw_calendar()
	{
		$id = $this->input->post('id');
		$month = $this->input->post('mes');
		$year = $this->input->post('ano');
		$calendar = $this->Magendamento->draw_calendar($id, $month, $year);
		echo $calendar;
	}

	public function cadastrar()
	{
		if ($this->input->post()) {
			$this->load->helper('utility');

			$horario = $this->input->post('horario');
			$numeroDeAtendimento = $this->input->post('numeroDeAtendimento');
			$dataLista = $this->input->post('data');
			$datas = explode(", ", $dataLista);

			for ($i=0; $i < count($datas); $i++) { 
				$array = array();
				for ($j=0; $j < count($horario); $j++) { 
					$array[$j] = array(
						'data' => get_data_for_mysql_format($datas[$i]),
						'horario' => $horario[$j],
						'idServico' => $this->input->post('idServico'),
						'idComercio' => $this->input->post('idComercio'),
						'numeroDeAtendimento' => $numeroDeAtendimento[$j]
					);
				}
				$query = $this->Magendamento->cadastrar($array);
			}

			$this->flashData($query['status']);
			redirect('cliente/Agendamento');
			
		}

		// meus dados
		$meusDados = $this->Mcomercio->meuComercio();
		
		$this->template->load('cliente/index', 'cliente/agendamento-cadastrar', array(
			'dropdownDeServico' => $this->Mcs->lista($meusDados['result'][0]->idComercio),
			'dropdownDeComercio' => $meusDados
		));	
	}

	public function excluir($id)
	{
		$query = $this->Magendamento->excluir($id);
		$this->flashData($query['status']);
		redirect('cliente/Agendamento');
	}

	public function flashData($status)
	{
		$this->session->set_flashdata('flashdata', array(
			'msg'  => (($status) ? 'Operação efetuada com sucesso!' : 'Erro ao efetuar operaçãdo.'),
			'type' => (($status) ? 'alert-success' : 'alert-warning')
		));
	}	

	public function get_detalhes()
	{
		$id = $this->input->post('id');
		$detalhes = $this->Magendamento->get_detalhes($id);
		echo json_encode($detalhes);
	}

	public function atualizar_status()
	{
		$id = $this->input->post('id');
		$status = $this->input->post('status');
		if ($this->Magendamento->atualizar_status($id, $status)) {
			$this->session->set_flashdata('flashdata', array(
				'msg'  => 'Status alterado com sucesso!',
				'type' => 'alert-success'
			));
		} else {
			$this->session->set_flashdata('flashdata', array(
				'msg'  => 'Erro ao alterar o status do agendamento!',
				'type' => 'alert-warning'
			));
		}
	}

	public function atualizar_info()
	{
		$id = $this->input->post('id');
		$data['valorPeca'] = $this->input->post('valorPeca');
		$data['valorServico'] = $this->input->post('valorServico');
		$data['obs'] = $this->input->post('obs');
		$this->Magendamento->atualizar_info($id, $data);
	}

	public function notifica_agendamento($id, $creator, $reader=false){
		$notificacao = array(
					  'id_tabela' => $id,
					  'tipo' => 'Agendamento',
					  'creator' => $creator
					);
		$this->Musuario->create_notification($notificacao, 'Root');
	    $this->Musuario->create_notification($notificacao, 'Cliente', $reader);
	}
}