<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

		$this->load->model(array(
			'Mstatus',
			'Musuario',
			'Mendereco',
			'Mcomercio',
			'Mservico',
			'Magendamento'
			));

		if ($this->Musuario->verificarNivel($this->session->userdata('idUsuario')) != 'Root') {
			$this->session->sess_destroy();
			redirect('dashboard/Login');
		}

		// initialize
		$this->upload->initialize(array(
			'upload_path' => 'common/data',
			'encrypt_name' => TRUE,
			'remove_spaces' => TRUE,
			'allowed_types' => 'jpg|jpeg'
			));
	}

	public function index()
	{	

		$qtdChart = $this->get_qtd_clientes_chart();
		$agendamentoChart 	= $this->get_line_charts($this->Magendamento->get_agendamento());
		$cadastro_usuarios  = $this->get_line_charts($this->Musuario->get_usuario_charts());
		$cadastro_clientes 	= $this->get_line_charts($this->Musuario->get_cliente_charts());
		$agendamento_periodo 	= $this->get_line_charts($this->Magendamento->get_agendamento_periodo_charts());

		// meus agendamentos
		$ag = $this->Magendamento->lista_agendamentos();
		$agendamento = array();
		for ($i=0; $i < count($ag['result']); $i++) {
			$cm = $this->Mcomercio->lista_cliente($ag['result'][$i]->idComServ);
			$se = $this->Mservico->lista_cliente($ag['result'][$i]->idComServ);

			switch ($ag['result'][$i]->status) {
				case 'Confirmado':
				case 'Concluido':
					$class = 'confirmed';
					break;
				case 'Pendente':
					$class = 'pending';
					break;
				case 'Cancelado':
					$class = 'canceled';
					break;
			}
						
			$agendamento[$i] = array(
				'id' => $ag['result'][$i]->id,
				'nome' => $ag['result'][$i]->nome . ' ' . $ag['result'][$i]->sobrenome,
				'data' => implode('/', array_reverse(explode('-', $ag['result'][$i]->data))),
				'horario' => $ag['result'][$i]->hora,
				'servico' => (isset($se['result'][0]->servico) ? $se['result'][0]->servico : ''),
				'comercio' => (isset($cm['result'][0]->razao) ? $cm['result'][0]->razao : ''),
				'dataHora' => $ag['result'][$i]->dataHora,
				'status' => $ag['result'][$i]->status,
				'class' => $class
			);
		}

		$this->template->load('dashboard/index', 'dashboard/relatorios', array(
			'lista' => (($agendamento != NULL) ? $agendamento : array()),
			'usuarios' => $this->Musuario->lista_usuarios(),
			'barChart' => $qtdChart,
			'agendamentoChart' => $agendamentoChart,
			'usuarioschart' => $cadastro_usuarios,
			'clienteschart' => $cadastro_clientes,
			'agendamentoPeriodo' => $agendamento_periodo,
		));
	}

	public function editar()
	{
		echo 'editar';
	}

	public function cadastrar()
	{
		if ($this->input->post()) {
			$this->form_validation->set_rules('nome', 'Nome', 'trim|required');
			$this->form_validation->set_rules('nivel', 'nivel', 'trim|required');
			$this->form_validation->set_rules('idStatus', 'IdStatus', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$senha = substr(md5(uniqid(rand(), true)), 0, 8);

			if (!$this->form_validation->run()) {
				echo validation_errors();
				exit();
			}

			if ((isset($_FILES['file'])) && ($_FILES['file']['size'] > 0)) {
				if (!$this->upload->do_upload('file')){
					echo $this->upload->display_errors();
					exit();
				}

				$upload = $this->upload->data();
				$foto = $upload['file_name'];
			} else {
				$foto = ((($this->input->post('fotoDaEmpresaHidden'))) ? $this->input->post('fotoDaEmpresaHidden') : NULL);
			}

			$usuario = $this->Musuario->cadastrar(array(
				'idUsuario' => (($this->input->post('idUsuario')) ? $this->input->post('idUsuario') : NULL),
				'nome' 	    => $this->input->post('nome'),
				'nivel'     => $this->input->post('nivel'),
				'email'     => $this->input->post('email'),
				'senha'     => $senha,
				'idStatus'  => $this->input->post('idStatus'),
				'telefone'  => $this->input->post('telefone')
				));

			if ($usuario['status']) {
				$comercio = $this->Mcomercio->cadastrar(array(
					'IdUsuario'       => $usuario['queryId'],
					'idComercio'      => (($this->input->post('idComercio')) ? $this->input->post('idComercio') : NULL),
					'planoEmergencia' => $this->input->post('idPlanoEmergencia'),
					'planoFidelidade' => $this->input->post('idPlanoFidelidade'),
					'planoFinanceiro' => $this->input->post('idPlanoFinanceiro'),
					'cnpj' 			  => $this->input->post('cnpj'),
					'razao' 		  => $this->input->post('razao'),
					'descricao'       => $this->input->post('descricao'),
					'fotoDaEmpresa'   => $foto
					));
			}

			if ($comercio['status']) {
				$endereco = $this->Mendereco->cadastrar(array(
					'idEndereco'  => (($this->input->post('idEndereco')) ? $this->input->post('idEndereco') : NULL),
					'idComercio'  => $comercio['queryId'],
					'cep'	      => $this->input->post('cep'),
					'endereco' 	  => $this->input->post('endereco'),
					'numero' 	  => $this->input->post('numero'),
					'complemento' => $this->input->post('complemento'),
					'bairro' 	  => $this->input->post('bairro'),
					'cidade' 	  => $this->input->post('cidade'),
					'estado' 	  => $this->input->post('estado')
					));
			}

			$this->flashData($usuario['status']);
			redirect('dashboard/Cliente');
		} 

		$this->template->load('dashboard/index', 'dashboard/cliente-cadastrar', array(
			'listaDeStatus' => $this->Mstatus->lista(array())
			));	
	}

	public function visualizar($id)
	{
		$usuario = $this->Musuario->lista(array(
			'where' => array('idUsuario' => $id)
			));

		// var_dump($usuario);
		// exit();

		$comercio = $this->Mcomercio->lista(array(
			'where' => array('idUsuario' => $id)
			));

		// var_dump($comercio);
		// exit();

		$endereco = $this->Mendereco->lista(array(
			'where' => array('idComercio' => $comercio['result'][0]->idComercio)
			));

		// var_dump($endereco);
		// exit();

		$this->template->load('dashboard/index', 'dashboard/cliente-cadastrar', array(
			'usuario' => $usuario,
			'comercio' => $comercio,
			'endereco' => $endereco,
			'listaDeStatus' => $this->Mstatus->lista(array())
			));	
	}

	// default

	public function flashData($status)
	{
		$this->session->set_flashdata('flashdata', array(
			'msg'  => (($status) ? 'Operação efetuada com sucesso!' : 'Erro ao efetuar operaçãdo.'),
			'type' => (($status) ? 'alert-success' : 'alert-warning')
			));
	}

	
	public function get_qtd_clientes_chart(){
		//-------------------- GRAFICO DE QUANTIDADE DE USUARIOS POR CLIENTES ------------------------

		//array com os nomes dos clientes
		$clientes = $this->Musuario->get_bar_charts();
		if(count($clientes) > 0){
			foreach ($clientes as $cliente) {
				$labels[] = $cliente->nome;
				$qtds[] = $cliente->quantidade;
			}
		}else{
			$qtds = array('0');
			$labels = array('0');			
		}

		//configurações do estilo dos graficos
		$datasets = array(
			array(
				"label" => "Quantidade de usuários cadastrados por Cliente",
				"fillColor" => "rgba(151,187,205,0.5",
					"strokeColor" => "rgba(151,187,205,0.5",
						"highlightFill" => "rgba(151,187,205,0.5",
							"highlightStroke" => "rgba(151,187,205,0.5",
								"data" => $qtds
								)
			);

		//junta tudo pra fazer um json object e passa no JS para criar o gráfico
		$data = array(
			"labels" => $labels,
			"datasets" =>$datasets
			);
		$barChart = json_encode($data);

		return $barChart;
	}



	public function get_line_charts($array){

		$labels = [
			date('d/m',mktime(0,0,0,date('m'), (date('d')-6), date('Y'))),
			date('d/m',mktime(0,0,0,date('m'), (date('d')-5), date('Y'))),
			date('d/m',mktime(0,0,0,date('m'), (date('d')-4), date('Y'))),
			date('d/m',mktime(0,0,0,date('m'), (date('d')-3), date('Y'))),
			date('d/m',mktime(0,0,0,date('m'), (date('d')-2), date('Y'))),
			date('d/m',mktime(0,0,0,date('m'), (date('d')-1), date('Y'))),
			date('d/m',mktime(0,0,0,date('m'), date('d'), date('Y'))),
		];
		foreach ($array as $a) {
			$index = array_search(date('d/m', strtotime($a->data)), $labels); 
			if($index !== false){
				$qtds[$index] = $a->qtd;
			}
		}

		//esse for preenche o resto dos indices que ficaram null com 0, porque não houveram agendamentos pra eles
		for($i=0;$i<7;$i++){
			if(!isset($qtds[$i])){
				$qtds[$i] = 0;
			}
		}

		//ordena por indice
		ksort($qtds);

		$datasets = array(
			array(
				"label" => "",
				"fillColor" => "rgba(151,187,205,0",
				"strokeColor" => "rgba(151,187,205,0.5",
				"pointColor" => "rgba(151,187,205,0.5",
				"pointStrokeColor" => "#fff",
				"pointHighlightFill" => "#fff",
				"pointHighlightStroke" => "rgba(151,187,205,0.5",
				"data" => $qtds
			)
		);

		$data_chart = array(
			"labels" => $labels,
			"datasets" =>$datasets
		);


		$Chart = json_encode($data_chart);

		return $Chart;

	}
}

/* End of file Cliente.php */
/* Location: ./application/controllers/Cliente.php */