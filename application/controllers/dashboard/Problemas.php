<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Problemas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

		$this->load->model(array(
			'Musuario',
			'Mproblema',
			'Mpc'
		));

		if ($this->Musuario->verificarNivel($this->session->userdata('idUsuario')) != 'Root') {
			$this->session->sess_destroy();
			redirect('dashboard/Login');
		}
	}

	public function index()
	{
		$this->template->load('dashboard/index', 'dashboard/problemas', array(
			'problemas' => $this->Mproblema->lista(array()),
			'comentarios' => $this->Mpc->lista(array())
		));
	}

	public function comentar()
	{
		if ($this->input->post()) {
	        $comentario = $this->Mpc->cadastrar(array(
				'idProblema' => $this->input->post('idProblema'),
				'idUsuario' => $this->input->post('idUsuario'),
				'comentario' 	    => $this->input->post('comentario'),
			));
			redirect('dashboard/Problemas');
		}
	}

	public function notifica_agendamento($id, $creator){
		$notificacao = array(
					  'id_tabela' => $id,
					  'tipo' => 'Problema',
					  'creator' => $creator
					);

	    $this->Musuario->create_notification($notificacao, 'Root');
	    
	}


}

/* End of file Problemas.php */
/* Location: ./application/controllers/comercio/Problemas.php */