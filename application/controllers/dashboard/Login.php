<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

		// model
		$this->load->model(array(
			'Musuario'
		));
	}

	public function index()
	{
		if ($this->input->post()) {
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('senha', 'Senha', 'trim|required');

			if (!$this->form_validation->run()) {
				echo validation_errors();
				exit();
			}

			$query = $this->Musuario->login($this->input->post());

			$this->flashData($query['numrows']);
			if ($query['numrows'] > 0) {
				$this->session->set_userdata('idUsuario', $query['result'][0]['idUsuario']);
				$this->session->set_userdata('tipo', $query['result'][0]['nivel']);
				//$this->session->set_userdata('user', $query['result'][0]['nome']);
				switch ($query['result'][0]['nivel']) {
					case 'Root': $view = 'dashboard/Home'; break;
					case 'Cliente': $view = 'cliente/Home'; break;
				}

				redirect($view);
			}
			$this->load->view('dashboard/login');
		}

		$this->load->view('dashboard/login');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('dashboard/Login');
	}

	public function flashData($status)
	{
		$this->session->set_flashdata('flashdata', array(
			'msg'  => ((($status > 0)) ? 'Seja bem-vindo!' : 'Erro ao efetuar login.'),
			'type' => ((($status > 0)) ? 'alert-success'   : 'alert-warning')
		));
	}
}