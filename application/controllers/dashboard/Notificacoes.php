<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificacoes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

		$this->load->model(array(
			'Musuario',
			'Magendamento',
			'Mavaliacoes',
			'Mservico',
			'Mcomercio'
		));

		if ($this->Musuario->verificarNivel($this->session->userdata('idUsuario')) != 'Root') {
			$this->session->sess_destroy();
			redirect('dashboard/Login');
		}
	}

	public function index()
	{
		$ag = $this->get_notifications_agendamento();
		$av = $this->get_notifications_avaliacoes();
		$cli = $this->get_notifications_clientes();
		$delete = $this->db->where('reader', $_SESSION['idUsuario'])->delete('notificacao');
		$this->template->load('dashboard/index', 'dashboard/notificacoes', array(
			'agendamentos' =>  $ag,
			'avaliacoes' =>  $av,
			'clientes' =>  $cli
			
			));	
	}

	public function get_notifications_agendamento(){
		$query = $this->db->query("select SA.data as data_agendamento,SA.hora AS hora_agendamento, SA.status, US.nome, COM.razao as cliente 
								   from servicos_agendados SA
								   inner join usuario US on SA.idUsuario = US.idUsuario 
								   inner join comercio_servico CS on CS.id = SA.idComServ
								   inner join comercio COM on CS.idComercio = COM.idComercio
								   
								   where SA.id in
								   (select id_tabela from notificacao where tipo = 'Agendamento'
								   	and reader = ".$_SESSION['idUsuario'].")")->result();
		      // echo '<pre>';
		      // var_dump($query);
		      // exit;
		return $query;
	}
	public function get_notifications_clientes(){
		$query = $this->db->query("select A.idUsuario, A.dataHora as data, A.nome as cliente_cadastrado, B.nome as quem_cadastrou
				 from usuario A inner join usuario B on A.cliente = B.idUsuario 
				 where A.idUsuario in
								   (select id_tabela from notificacao where tipo = 'Cliente'
								   	and reader = ".$_SESSION['idUsuario'].")")->result();
    
		return $query;
	}
	
	public function get_notifications_avaliacoes(){
		$query = $this->db->query("select AV.comentario, AV.dataHora as data, US.nome as usuario, COM.razao as cliente from avaliacoes AV 
					inner join usuario US ON US.idUsuario = AV.idUsuario
					inner join comercio COM on COM.idComercio = AV.idCliente 
				 	where AV.id in
								   (select id_tabela from notificacao where tipo = 'Avaliacoes'
								   	and reader = ".$_SESSION['idUsuario'].")")->result();
		return $query;
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */