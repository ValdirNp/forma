<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servico extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

		$this->load->model(array(
			'Musuario',
			'Mservico'
		));

		if ($this->Musuario->verificarNivel($this->session->userdata('idUsuario')) != 'Root') {
			$this->session->sess_destroy();
			redirect('dashboard/Login');
		}
	}

	public function index()
	{
		$this->template->load('dashboard/index', 'dashboard/servico', array(
			'lista' => $this->Mservico->lista(array('where' => array()))
		));
	}

	public function cadastrar()
	{
		if ($this->input->post()) {
			$servico = $this->Mservico->cadastrar($this->input->post());
			$this->flashData($servico['status']);
			$notificacao = array(
					  'id_tabela' => $servico['id'],
					  'tipo' => 'Serviço',
					  'creator' => $_SESSION['idUsuario']
					);

	        	$this->Musuario->create_notification($notificacao, 'Root');
			redirect('dashboard/Servico');
		}

		$this->template->load('dashboard/index', 'dashboard/servico-cadastrar', array());	
	}

	public function visualizar($id)
	{
		$this->template->load('dashboard/index', 'dashboard/servico-cadastrar', array(
			'servico' => $this->Mservico->lista(array('where' => array('idServico' => $id)))
		));	
	}

	// default

	public function flashData($status)
	{
		$this->session->set_flashdata('flashdata', array(
			'msg'  => (($status) ? 'Operação efetuada com sucesso!' : 'Erro ao efetuar operaçãdo.'),
			'type' => (($status) ? 'alert-success' : 'alert-warning')
		));
	}

}

/* End of file Servico.php */
/* Location: ./application/controllers/comercio/Servico.php */