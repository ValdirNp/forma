<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

		$this->load->model(array(
			'Mstatus',
			'Musuario',
			'Mendereco',
			'Mcomercio',
			'Mservico',
			'Msu'
		));

		if ($this->Musuario->verificarNivel($this->session->userdata('idUsuario')) != 'Root') {
			$this->session->sess_destroy();
			redirect('dashboard/Login');
		}

		// initialize
		$this->upload->initialize(array(
			'upload_path' => 'common/data',
			'encrypt_name' => TRUE,
			'remove_spaces' => TRUE,
			'allowed_types' => 'jpg|jpeg'
		));
	}

	public function index()
	{
		$listaDeAtivos = $this->Musuario->lista_clientes($status = 1);

		$listaDePendentes = $this->Musuario->lista_clientes($status = 3);
		
		$this->template->load('dashboard/index', 'dashboard/cliente', array(
			'listaDeAtivos' => $listaDeAtivos,
			'listaDePendentes' => $listaDePendentes
		));
	}

	public function editar()
	{
		echo 'editar';
	}

	public function cadastrar()
	{
		if ($this->input->post()) {
			$this->form_validation->set_rules('nome', 'Nome', 'trim|required');
			$this->form_validation->set_rules('nivel', 'nivel', 'trim|required');
			$this->form_validation->set_rules('idStatus', 'IdStatus', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$senha = substr(md5(uniqid(rand(), true)), 0, 8);

			if (!$this->form_validation->run()) {
				echo validation_errors();
				exit();
			}

			if ((isset($_FILES['file'])) && ($_FILES['file']['size'] > 0)) {
				if (!$this->upload->do_upload('file')){
	                echo $this->upload->display_errors();
	                exit();
	            }

	            $upload = $this->upload->data();
	            $foto = $upload['file_name'];
	        } else {
	        	$foto = ((($this->input->post('fotoDaEmpresaHidden'))) ? $this->input->post('fotoDaEmpresaHidden') : NULL);
	        }


	        $usuario = $this->Musuario->cadastrar(array(
				'idUsuario' => (($this->input->post('idUsuario')) ? $this->input->post('idUsuario') : NULL),
				'nome' 	    => $this->input->post('nome'),
				'nivel'     => $this->input->post('nivel'),
				'email'     => $this->input->post('email'),
				'senha'     => $senha,
				'idStatus'  => $this->input->post('idStatus'),
				'telefone'  => $this->input->post('telefone'),
				'telEmergencia'  => $this->input->post('telEmergencia'),
				'cliente'   => $_SESSION['idUsuario']	
			));
			      
	        if($this->input->post('nivel') == 'Cliente'){
	        	$notificacao = array(
					  'id_tabela' => $usuario['queryId'],
					  'tipo' => 'Cliente',
					  'creator' => $_SESSION['idUsuario']
					);

	        	$this->Musuario->create_notification($notificacao, 'Root');
	        }
	        if ($usuario['status']) {
				$comercio = $this->Mcomercio->cadastrar(array(
					'IdUsuario'       => $usuario['queryId'],
					'idComercio'      => (($this->input->post('idComercio')) ? $this->input->post('idComercio') : NULL),
					'planoEmergencia' => $this->input->post('idPlanoEmergencia'),
					'planoFidelidade' => $this->input->post('idPlanoFidelidade'),
					'planoFinanceiro' => $this->input->post('idPlanoFinanceiro'),
					'cnpj' 			  => $this->input->post('cnpj'),
					'razao' 		  => $this->input->post('razao'),
					'descricao'       => $this->input->post('descricao'),
					'fotoDaEmpresa'   => $foto
					
				));
			}

			if ($comercio['status']) {
				$endereco = $this->Mendereco->cadastrar(array(
					'idEndereco'  => (($this->input->post('idEndereco')) ? $this->input->post('idEndereco') : NULL),
					'idComercio'  => $comercio['queryId'],
					'cep'	      => $this->input->post('cep'),
					'endereco' 	  => $this->input->post('endereco'),
					'numero' 	  => $this->input->post('numero'),
					'complemento' => $this->input->post('complemento'),
					'bairro' 	  => $this->input->post('bairro'),
					'cidade' 	  => $this->input->post('cidade'),
					'estado' 	  => $this->input->post('estado'),
					'latitude' 	  => $this->input->post('latitude'),
					'longitude'   => $this->input->post('longitude')
				));
			}

        	$limparServicosUsuarios = $this->Msu->deleteByidUser($usuario['queryId']);
			foreach($this->input->post('servicos') as $servico){
				$servicosUsuarios = $this->Msu->cadastrar(array(
					'idUsuario' => $usuario['queryId'],
					'idServico' => $servico
				));
			}

			$vencimento = $this->input->post('vencimento');
			$pago = $this->input->post('pago');
			$idUsuario = $this->input->post('idUsuario');

			$this->Musuario->atualizar_pagamento($vencimento, $pago, $idUsuario);

			$this->flashData($usuario['status']);
			redirect('dashboard/Cliente/visualizar/' . $idUsuario);
		}

		$servicos = $this->Mservico->lista(array(
			'where' => array('status' => 'On-Line')
		));

		$pagamentos = array('result' => array());
		$this->template->load('dashboard/index', 'dashboard/cliente-cadastrar', array(
			'servicos' => $servicos,
			'listaDeStatus' => $this->Mstatus->lista(array()),
			'pagamentos' => $pagamentos
		));
	}

	public function visualizar($id)
	{
		$usuario = $this->Musuario->lista(array(
			'where' => array('`usuario`.`idUsuario`' => $id)
		));

		// var_dump($usuario);
		// exit();

		$comercio = $this->Mcomercio->lista(array(
			'where' => array('idUsuario' => $id)
		));

		// var_dump($comercio);
		// exit();

		$endereco = $this->Mendereco->lista(array(
			'where' => array('idComercio' => $comercio['result'][0]->idComercio)
		));

		// var_dump($endereco);
		// exit();

		$servicos = $this->Mservico->lista(array(
			'where' => array('status' => 'On-Line')
		));

		$servicos_cadastrados_query = $this->Msu->lista(array(
			'where' => array('idUsuario' => $id)
		));

		$servicos_cadastrados = array();
		foreach($servicos_cadastrados_query['result'] as $servico){
			$servicos_cadastrados[] = $servico->idServico;
		}

		$pagamentos = $this->Musuario->lista_pagamentos(array(
			'where' => array('idUsuario' => $id)
		));

		foreach ($pagamentos['result'] as $pagamento) {
			list($ano, $mes, $dia) = explode('-', $pagamento->vencimento);
			$pagamento->vencimento = $dia . '/' . $mes . '/' . $ano;
			$pagamento->pago = ($pagamento->pago == 'S') ? 'Sim' : 'Não';
		}

		$this->template->load('dashboard/index', 'dashboard/cliente-cadastrar', array(
			'usuario' => $usuario,
			'comercio' => $comercio,
			'endereco' => $endereco,
			'servicos' => $servicos,
			'servicos_cadastrados' => $servicos_cadastrados,
			'listaDeStatus' => $this->Mstatus->lista(array()),
			'pagamentos' => $pagamentos
		));
	}

	// default

	public function flashData($status)
	{
		$this->session->set_flashdata('flashdata', array(
			'msg'  => (($status) ? 'Operação efetuada com sucesso!' : 'Erro ao efetuar operaçãdo.'),
			'type' => (($status) ? 'alert-success' : 'alert-warning')
		));
	}
}

/* End of file Cliente.php */
/* Location: ./application/controllers/Cliente.php */