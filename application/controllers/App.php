<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('app_model');
		$this->load->helper('utility');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
		header('Access-Control-Allow-Methods: GET, POST, PUT');
		//http://stackoverflow.com/questions/15485354/angular-http-post-to-php-and-undefined
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)) $_POST = json_decode(file_get_contents('php://input'), true);

		$this->upload->initialize(array(
			'upload_path' => 'common/data',
			'encrypt_name' => TRUE,
			'remove_spaces' => TRUE,
			'allowed_types' => 'jpg|jpeg'
		));
	}

	public function index()
	{
	}

	public function cadastra_usuario() //ok
	{
		$dados = $this->input->post();
		// if ((isset($_FILES['foto'])) && ($_FILES['foto']['size'] > 0)) {
		// 	if (!$this->upload->do_upload('foto')){
  //               echo $this->upload->display_errors();
  //               exit();
  //           }

  //           $upload = $this->upload->data();
  //           $dados['foto'] = $upload['file_name'];
  //       }
		$imagem = str_replace('data:image/jpg;base64,', '', $dados['foto']);
		$imagem = str_replace('data:image/jpeg;base64,', '', $dados['foto']);
		$imagem = str_replace(' ', '+', $imagem);
		$imagem = base64_decode($imagem);
		$nome = 'common/data/'. md5(uniqid(rand(), true)) . '.jpg';
		$source_img = imagecreatefromstring($imagem);
		$imageSave = imagejpeg($source_img, $nome, 100);
		if ($imageSave == FALSE) {
			unset($dados['foto']);
		} else {
			$dados['foto'] = str_replace('common/data/', '', $nome);
		}

		$usuario = $this->app_model->cadastra_usuario($dados);
		if ($usuario == FALSE) {
			echo json_encode(FALSE);
		} else {
			$this->load->library('email');
			$mensagem = "Olá " . $dados['nome'] . ",<br />";
			$mensagem = "Cadastro efetuado no ClickMec!<br />";
			$mensagem .= "Sua senha de acesso ao ClickMec é: " . $dados['senha'] . "<br />";
	
			$config['charset'] = 'utf-8';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = 'html';
			$this->email->initialize($config);

			$this->email->from('clickmec@diagramando.com.br', 'ClickMec');
			$this->email->to($dados['email']);
			$this->email->subject("Cadastro - ClickMec");
			$this->email->message($mensagem);
			$this->email->send();
			echo json_encode($usuario);
		}
	}

	public function edita_usuario() //ok
	{
		$dados = $this->input->post();
		
		$imagem = str_replace('data:image/jpg;base64,', '', $dados['foto']);
		$imagem = str_replace('data:image/jpeg;base64,', '', $dados['foto']);
		$imagem = str_replace(' ', '+', $imagem);
		$imagem = base64_decode($imagem);
		$nome = 'common/data/'. md5(uniqid(rand(), true)) . '.jpg';
		$source_img = imagecreatefromstring($imagem);
		$imageSave = imagejpeg($source_img, $nome, 100);
		if ($imageSave == FALSE) {
			unset($dados['foto']);
		} else {
			$dados['foto'] = str_replace('common/data/', '', $nome);
		}
		// echo "<pre>";
		// var_dump($dados['foto']);
		// exit;
		$usuario = $this->app_model->edita_usuario($dados);
		if ($usuario == FALSE) {
			echo json_encode(FALSE);
		} else {
			echo json_encode($usuario);
		}
	}

	public function get_dados_cliente($id) //ok
	{
		$dados = $this->app_model->get_dados_cliente($id);
		echo json_encode($dados);
	}

	public function get_servicos_cliente($id) //ok
	{
		$servicos = $this->app_model->get_servicos_cliente($id);
		echo json_encode($servicos);
	}

	public function get_servicos() //ok
	{
		$servicos = $this->app_model->get_servicos(FALSE);
		echo json_encode($servicos);
	}

	public function get_servicos_emergencia() //ok
	{
		$servicos = $this->app_model->get_servicos(TRUE);
		echo json_encode($servicos);
	}

	public function get_avaliacoes($id) //ok
	{
		$avaliacoes = $this->app_model->get_avaliacoes($id);
		echo json_encode($avaliacoes);
	}

	public function avaliar() //ok
	{
		$success = array('status' => TRUE, 'message' => 'Avaliação feita com sucesso!');
	    $error = array('status' => FALSE, 'message' => 'Erro ao avaliar a oficina!  Tente novamente!');
		if ($_POST) {
			$dados = $this->input->post();
			if ($this->app_model->avaliar($dados)) {
				echo json_encode($success);
			} else {
				echo json_encode($error);
			}
		} else {
			echo json_encode($error);
		}
	}

	public function get_clientes() //ok
	{
		$dados = $this->input->post();
		$clientes = $this->app_model->get_clientes(NULL, FALSE, NULL, $dados);
		echo json_encode($clientes);
	}

	public function busca_oficinas()
	{
			$error = array('status' => FALSE, 'message' => 'Erro ao agendar o serviço! Tente novamente!');
		if ($_POST) {
			$dados = $this->input->post('string');
			$clientes = $this->app_model->get_clientes(NULL, FALSE, $dados);
			echo json_encode($clientes);
		} else {
		}	echo json_encode($error);
	}

	public function get_clientes_servico($id) //ok
	{
		$clientes = $this->app_model->get_clientes($id, FALSE);
		echo json_encode($clientes);
	}

	public function get_clientes_servico_emergencia($id) //ok
	{
		$clientes = $this->app_model->get_clientes($id, TRUE);
		echo json_encode($clientes);
	}

	public function get_horarios($id) //ok
	{
		$horarios = $this->app_model->get_horarios($id);
		echo json_encode($horarios);
	}



	public function agendar() //ok
	{
		$empty = array('status' => FALSE, 'message' => 'Horário não disponível!');        
        $success = array('status' => TRUE, 'message' => 'Serviço agendado com sucesso!');        
        $error = array('status' => FALSE, 'message' => 'Erro ao agendar o serviço! Tente novamente!');
        if ($_POST) {
			$dados = $this->input->post();
			$agendamento = $this->app_model->agendar($dados);
			if (empty($agendamento)) {
				echo json_encode($empty);
			} else {
				$this->load->library('email');
				$mensagem = "Olá,<br />";
				$mensagem .= "Um novo serviço foi agendado para você!<br />";
				$mensagem .= "Data: " . format_data_from_mysql($agendamento->data) . "<br />";
				$mensagem .= "Horário: " . format_hora_from_mysql($agendamento->hora) . "<br />";
				$mensagem .= "Serviço: " . $agendamento->servico . "<br />";
				$mensagem .= "Oficina: " . $agendamento->oficina . "<br />";
				$mensagem .= "Endereço: " . $agendamento->endereco . "<br />";
		
				$config['charset'] = 'utf-8';
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = 'html';
				$this->email->initialize($config);

				$this->email->from('clickmec@diagramando.com.br', 'ClickMec');
				$this->email->to($agendamento->email);
				$this->email->subject("Agendamento - ClickMec");
				$this->email->message($mensagem);
				$this->email->send();
				echo json_encode($success);
				// echo json_encode($agendamento);
			}
		} else {
			echo json_encode($error);
		}
	}

	public function get_oficinas_proximas()
	{
        $error = array('status' => FALSE, 'message' => 'Erro ao listar as oficinas próximas! Tente novamente!');
        if ($_POST) {
        	$dados = $this->input->post();
			$proximas = $this->app_model->get_proximas($dados, FALSE);
			echo json_encode($proximas);
		} else {
			echo json_encode($error);
		}
	}

	public function get_oficinas_proximas_emergencia()
	{
        $error = array('status' => FALSE, 'message' => 'Erro ao listar as oficinas próximas! Tente novamente!');
        if ($_POST) {
        	$dados = $this->input->post();
			$proximas = $this->app_model->get_proximas($dados, TRUE);
			echo json_encode($proximas);
		} else {
			echo json_encode($error);
		}
	}

	public function get_clientes_emergencia() //ok
	{
		$oficinas = $this->app_model->get_clientes(NULL, TRUE);
		echo json_encode($oficinas);
	}

	public function get_historico_usuario($id) //ok
	{
		$historico = $this->app_model->get_historico_usuario($id);
		echo json_encode($historico);
	}

	public function filtra_historico() //ok
	{
		$error = array('status' => FALSE, 'message' => 'Erro ao filtrar o histórico! Tente novamente!');
        if ($_POST) {
        	$dados = $this->input->post();
			$historico = $this->app_model->filtra_historico($dados);
			echo json_encode($historico);
		} else {
			echo json_encode($error);
		}
	}

	public function login() //ok
	{
		$error = array('status' => FALSE, 'message' => 'Erro ao fazer login! Tente novamente!');
		if ($_POST) {
			$dados = $this->input->post();
			$usuario = $this->app_model->login($dados);
			echo json_encode($usuario);
		} else {
			echo json_encode($error);
		}
	}

	public function set_status_push() //ok
	{
		$error = array('status' => FALSE, 'message' => 'Erro ao atualizar status das notificações! Tente novamente!');
		$success = array('status' => TRUE, 'message' => 'Status das notificações push alterado!');
		if ($_POST) {
			$dados = $this->input->post();
			if ($this->app_model->set_status_push($dados)) {
				echo json_encode($success);
			} else {
				echo json_encode($error);
			}
		} else {
			echo json_encode($error);
		}
	}

	public function set_status_email() //ok
	{
		$error = array('status' => FALSE, 'message' => 'Erro ao atualizar status das notificações por email! Tente novamente!');
		$success = array('status' => TRUE, 'message' => 'Status das notificações por email alterado!');
		if ($_POST) {
			$dados = $this->input->post();
			if ($this->app_model->set_status_email($dados)) {
				echo json_encode($success);
			} else {
				echo json_encode($error);
			}
		} else {
			echo json_encode($error);
		}
	}

	public function set_status_sms() //ok
	{
		$error = array('status' => FALSE, 'message' => 'Erro ao atualizar status das notificações por sms! Tente novamente!');
		$success = array('status' => TRUE, 'message' => 'Status das notificações por sms alterado!');
		if ($_POST) {
			$dados = $this->input->post();
			if ($this->app_model->set_status_sms($dados)) {
				echo json_encode($success);
			} else {
				echo json_encode($error);
			}
		} else {
			echo json_encode($error);
		}
	}

	public function verifica_servico_concluido()
	{
		$error = array('status' => FALSE);
		$success = array('status' => TRUE);
		if ($_POST) {
			$dados = $this->input->post();
			if ($this->app_model->verifica_servico_concluido($dados)) {
				echo json_encode($success);
			} else {
				echo json_encode($error);
			}
		} else {
			echo json_encode($error);
		}
	}

	// métodos para cadastrar carro
	public function get_marcas()
	{
		$marcas = $this->app_model->get_marcas();
		echo json_encode($marcas);
	}

	public function get_modelos($marca)
	{
		$modelos = $this->app_model->get_modelos($marca);
		echo json_encode($modelos);
	}

	public function get_anos($modelo)
	{
		$anos = $this->app_model->get_anos($modelo);
		echo json_encode($anos);
	}

	public function cadastrar_carro()
	{
		$error = array('status' => FALSE, 'message' => 'Erro ao cadastrar o carro! Tente novamente!');
		$success = array('status' => TRUE, 'message' => 'Carro cadastrado com sucesso!');
		if ($_POST) {
			$dados = $this->input->post();
			if ($this->app_model->cadastrar_carro($dados)) {
				echo json_encode($success);
			} else {
				echo json_encode($error);
			}
		} else {
			echo json_encode($error);
		}
	}

	public function excluir_carro($id)
	{
		$error = array('status' => FALSE, 'message' => 'Erro ao excluir o carro! Tente novamente!');
		$success = array('status' => TRUE, 'message' => 'Carro excluído com sucesso!');
		if ($this->app_model->excluir_carro($id)) {
			echo json_encode($success);
		} else {
			echo json_encode($error);
		}
	}

	public function listar_carros($id)
	{
		$carros = $this->app_model->listar_carros($id);
		echo json_encode($carros);
	}



	// métodos pra agendar
	public function verifica_disponibilidade($id) //ok
	{
		$error = array('status' => FALSE);
		$success = array('status' => TRUE);
		if($this->app_model->verifica_disponibilidade($id)){
			echo json_encode(TRUE);
		} else {
			echo json_encode(FALSE);
		}
	}

	public function get_datas($id) //ok
	{
		$horarios = $this->app_model->get_datas($id);
		echo json_encode($horarios);
	}

	public function get_servicos_data() //ok
	{
		$error = array('status' => FALSE, 'message' => 'Erro ao listar horários e serviços! Tente novamente!');
		if ($_POST) {
			$dados = $this->input->post();
			$servicos = $this->app_model->get_servicos_data($dados);
			echo json_encode($servicos);
		} else {
			echo json_encode($error);
		}
	}

	public function reportar() //ok
	{
		$success = array('status' => TRUE, 'message' => 'Comentário enviado!');
		$error = array('status' => FALSE, 'message' => 'Erro ao avaliar o app! Tente novamente!');
		if ($_POST) {
			$dados = $this->input->post();
			if($this->app_model->reportar($dados)) {
				echo json_encode($success);
			} else {
				echo json_encode($error);
			}
		} else {
			echo json_encode($error);
		}
	}	

	public function recuperar_senha() //ok
	{
		$error = array('status' => FALSE, 'message' => 'Erro ao recuperar a senha! Tente novamente!');
		$success = array('status' => TRUE, 'message' => 'Senha enviada por email!');
		if ($_POST) {
			$dados = $this->input->post();										
			$senha = $this->app_model->recuperar_senha($dados);
			if (empty($senha)) {
				echo json_encode($error);
			} else {
				$this->load->library('email');
				$mensagem = "Olá,<br />";
				$mensagem .= "Sua senha de acesso ao ClickMec é: " . $senha . "<br />";
		
				$config['charset'] = 'utf-8';
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = 'html';
				$this->email->initialize($config);

				$this->email->from('clickmec@diagramando.com.br', 'ClickMec');
				$this->email->to($dados['email']);
				$this->email->subject("Recuperação de senha - ClickMec");
				$this->email->message($mensagem);
				if ($this->email->send()) {
					echo json_encode($success);
				} else {
					echo json_encode($error);
				}
			}
		} else {
			echo json_encode($error);
		}
	}	

	public function get_servicos_disponiveis($idOficina)
	{
		$servicos = $this->app_model->get_servicos_disponiveis($idOficina);
		echo json_encode($servicos);
	}

	public function get_datas_servico() //ok
	{
		$error = array('status' => FALSE, 'message' => 'Erro ao listar horários e serviços! Tente novamente!');
		if ($_POST) {
			$dados = $this->input->post();
			$datas = $this->app_model->get_datas_servico($dados);
			echo json_encode($datas);
		} else {
			echo json_encode($error);
		}
	}
}

// Outgoing Server:	diagramando.com.br
// SMTP Port: 465