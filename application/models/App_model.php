<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_model extends CI_Model {
	public function cadastra_usuario($data)
    {   
        $msg = array('status' => FALSE, 'message' => 'Erro ao efetuar cadastro! Tente novamente');
        $double = array('status' => FALSE, 'message' => 'Erro ao efetuar cadastro! Usuário já existe');
        $data['nivel'] = 'Usuario';
        // $data['destaque'] = 'Não';
        $this->db->where('email', $data['email']);
        $this->db->limit(1);
        $query = $this->db->get('usuario');
        if ($query->num_rows() == 0) {
            if($this->db->insert('usuario', $data)){
                $this->db->where('email', $data['email']);
                $this->db->limit(1);
                $usuario = $this->db->get('usuario')->row();
                $usuario->status = TRUE;
                $usuario->foto = base_url() . 'common/data/' . $usuario->foto;
                return $usuario;
            } else {
                return $msg;
            }
        } else {
            if ($query->row()->provider_id == NULL && isset($data['provider_id'])) {
                $this->db->where('email', $data['email']);
                $this->db->set('provider_id', $data['provider_id']);
                if ($this->db->update('usuario')) {
                    $this->db->where('email', $data['email']);
                    $this->db->limit(1);
                    $usuario = $this->db->get('usuario')->row();
                    $usuario->status = TRUE;
                    $usuario->foto = base_url() . 'common/data/' . $usuario->foto;
                    return $usuario;
                } else {
                    return $double;
                }
            } else {
                return $double;
            }
        }
    }

    public function edita_usuario($data)
    {   
        $msg = array('status' => FALSE, 'message' => 'Erro ao atualizar cadastro! Tente novamente');
        $double = array('status' => FALSE, 'message' => 'Erro ao efetuar cadastro! Usuário já existe');
        $this->db->where('idUsuario', $data['idUsuario']);
        $query = $this->db->get('usuario');
        if ($query->num_rows() == 1) {
            $this->db->where('idUsuario', $data['idUsuario']);
            if($this->db->update('usuario', $data)){
                $this->db->where('idUsuario', $data['idUsuario']);
                $usuario = $this->db->get('usuario')->row();
                $usuario->status = TRUE;
                $usuario->foto = base_url() . 'common/data/' . $usuario->foto;
                return $usuario;
            } else {
                return $msg;
            }
        } else {
            return $msg;
        }
    }

    public function get_clientes($idServico = NULL, $emergencia = FALSE, $string = NULL, $dados)
    {
        $msg = array('status' => FALSE, 'message' => 'Erro ao listar oficinas! Tente novamente!');

        $this->db->select('comercio.*, usuario.destaque as feature, endereco.*, usuario.telefone, usuario.celular, usuario.telEmergencia, usuario.email');
        $this->db->from('comercio');
        $this->db->join('usuario', 'comercio.idUsuario = usuario.idUsuario', 'left');
        $this->db->join('endereco', 'comercio.idComercio = endereco.idComercio', 'left');
        if ($idServico != NULL) {
            $this->db->where('`comercio`.`idComercio` in (select `idUsuario` from `servico_usuario` where `idServico` = ' . $idServico . ')', NULL, FALSE);
        }
        if ($emergencia == TRUE) {
            $this->db->where('comercio.planoEmergencia', 'Sim');
        }
        if ($string == TRUE) {
            $this->db->like('comercio.razao', $string);
        }
        $this->db->order_by('usuario.destaque');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $clientes = $query->result();
            foreach ($clientes as $cliente) {
                if (isset($dados['latitude']) && isset($dados['longitude'])) {
                    $distancia = $this->calcDistancia($dados['latitude'], $dados['longitude'], $cliente->latitude, $cliente->longitude);
                    $cliente->distancia = round($distancia, 2) . ' km';
                }
                $cliente->status = TRUE;
                $cliente->fotoDaEmpresa = base_url() . 'common/data/' . $cliente->fotoDaEmpresa;
                $this->db->select('servico');
                $this->db->where('`idServico` in (select `idServico` from `servico_usuario` where `idUsuario` = ' .$cliente->idComercio. ')', NULL, FALSE);
                $new = $this->db->get('servico');
                if ($new->num_rows() > 0) {
                    $servicos = $new->result();
                    foreach ($servicos as $key => $servico) {
                        $array[$key] = $servico->servico;
                    }
                    $cliente->servicos = implode(', ', $array);
                } else {
                    $cliente->servicos = '';
                } 
            }        
            return $clientes;
        } else {
            return $msg;
        }
    }

    public function get_dados_cliente($id)
    {
        $msg = array('status' => FALSE, 'message' => 'Erro ao listar oficinas! Tente novamente!');
        $this->db->select('comercio.*, usuario.destaque as feature, usuario.telefone, usuario.celular, usuario.telEmergencia, endereco.*, usuario.email');
        $this->db->from('comercio');
        $this->db->join('usuario', 'comercio.idUsuario = usuario.idUsuario', 'left');
        $this->db->join('endereco', 'comercio.idUsuario = endereco.idComercio', 'left');
        $this->db->where('comercio.idComercio', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $cliente = $query->row();
            $cliente->status = TRUE;
            $cliente->fotoDaEmpresa = base_url() . 'common/data/' . $cliente->fotoDaEmpresa;
            return $cliente;
        }
        return $msg;
    }

    public function get_servicos_cliente($id)
    {
        $msg = array('status' => FALSE, 'message' => 'Erro ao listar serviços! Tente novamente!');

        $this->db->where('`servico`.`idServico` in (select `idServico` from `servico_usuario` where `idUsuario` = ' .$id. ')', NULL, FALSE);
        $query = $this->db->get('servico');
        if ($query->num_rows() > 0) {
            $servicos = $query->result();
            foreach ($servicos as $servico) {
                $this->db->where('idUsuario', $id);
                $this->db->where('idServico', $servico->idServico);
                $new = $this->db->get('servico_usuario');
                if ($new->num_rows() > 0) {
                    $servico->idComServ = $new->row()->id;
                }
                $servico->status = TRUE;
            }
            return $servicos;
        }
        return $msg;
    }

    public function get_servicos($emergencia = FALSE)
    {
        $msg = array('status' => FALSE, 'message' => 'Erro ao listar serviços! Tente novamente!');
        if ($emergencia == TRUE) {
            $this->db->where('`idServico` in (select `idServico` from `servico_usuario` where `idUsuario` in (select `idComercio` from `comercio` where `planoEmergencia` = "Sim"))');
        }
        $query = $this->db->get('servico');
        if ($query->num_rows() > 0) {
            $servicos = $query->result();
            foreach ($servicos as $servico) {
                $servico->status = TRUE;
            }
            return $servicos;
        }
        return $msg;
    }

    public function get_avaliacoes($id)
    {
        $msg = array('status' => FALSE, 'message' => 'Erro ao listar avaliações! Tente novamente!');
        $empty = array('status' => FALSE, 'message' => 'Cliente não possui avaliações!');
        $this->db->where('idCliente', $id);
        $this->db->where('status', 'Aprovado');
        $query = $this->db->get('avaliacoes');
        if ($query->num_rows() > 0) {
            $avaliacoes = $query->result();
            foreach ($avaliacoes as $avaliacao) {
                $this->db->select('idUsuario, nome, foto as avatar');
                $this->db->where('idUsuario', $avaliacao->idUsuario);
                $avaliacao->usuario = $this->db->get('usuario')->row();
                $avaliacao->status = TRUE;
                $avaliacao->usuario->avatar = base_url() . 'common/data/' . $avaliacao->usuario->avatar;
                unset($avaliacao->idUsuario);
            }                    
            return $avaliacoes;
        } else {
            return $empty;
        }
        return $msg;
    }

    public function avaliar($data)
    {
        $nota = $data['nota'];
        
        if ($this->db->insert('avaliacoes', $data)) {
            $this->db->where('idComercio', $data['idCliente']);
            $this->db->limit(1);
            $count = $this->db->get('comercio')->row()->countRate;
            $rate = $this->db->get('comercio')->row()->rate;
            $newRate = ($count * $rate) + $nota;
            $newRate = round($newRate/($count + 1));
            $this->db->where('idComercio', $data['idCliente']);
            $this->db->set('countRate', $count+1);
            $this->db->set('rate', $newRate);
            $this->db->update('comercio');
            return TRUE;
        }
        return FALSE;
    }

    public function get_horarios($id)
    {
        $msg = array('status' => FALSE, 'message' => 'Erro ao listar os horários dessa oficina! Tente novamente!');
        $empty = array('status' => FALSE, 'message' => 'Não há horários disponíveis nessa oficina!');
        $this->db->select('agendamento.*, servico.servico');
        $this->db->from('agendamento');
        $this->db->join('servico', 'agendamento.idServico = servico.idServico', 'left');
        $this->db->where('agendamento.idComercio', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $horarios = $query->result();
            foreach ($horarios as $horario) {
                $horario->status = TRUE;
                $this->db->where('idUsuario', $horario->idComercio);
                $this->db->where('idServico', $horario->idServico);
                $join = $this->db->get('servico_usuario')->row();
                $horario->idComServ = $join->id;
            }
            return $horarios;
        } else {
            return $empty;
        }
        return $msg;
    }

    public function agendar($dados)
    {
        
        $this->db->where('horario', $dados['hora']);
        $this->db->where('data', $dados['data']);
        $this->db->where('`idComercio` in (select `idUsuario` from `servico_usuario` where `id` = ' . $dados['idComServ'] . ')', NULL, FALSE);
        $this->db->where('`idServico` in (select `idServico` from `servico_usuario` where `id` = ' . $dados['idComServ'] . ')', NULL, FALSE);
        $query = $this->db->get('agendamento'); 
        if ($query->num_rows() > 0) {
            $horario = $query->row();
            $qtd = $horario->numeroDeAtendimento;
            if ($qtd == 1) {
                $this->db->where('idAgendamento', $horario->idAgendamento);
                $this->db->delete('agendamento');
            } else {
                $this->db->where('idAgendamento', $horario->idAgendamento);
                $this->db->set('numeroDeAtendimento', $qtd - 1);
                $this->db->update('agendamento');
            }

            if ($this->db->insert('servicos_agendados', $dados)) {
                $id = $this->db->insert_id();
                $this->db->where('id', $id);
                $agendado = $this->db->get('servicos_agendados')->row();
                $this->db->where('id', $agendado->idComServ);
                $new = $this->db->get('servico_usuario');
                $agendado->idServico = $new->row()->idServico;
                $agendado->idComercio = $new->row()->idUsuario;

                $this->db->where('idServico', $agendado->idServico);
                $newq = $this->db->get('servico');
                $agendado->servico = $newq->row()->servico;

                $this->db->where('idComercio', $agendado->idComercio);
                $newc = $this->db->get('comercio');
                $agendado->oficina = $newc->row()->razao;

                $this->db->where('idComercio', $agendado->idComercio);
                $newe = $this->db->get('endereco');
                $agendado->endereco = $newe->row()->endereco; 

                $this->db->where('idUsuario', $agendado->idUsuario);               
                $newu = $this->db->get('usuario');
                $agendado->email = $newu->row()->email;
                return $agendado;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    public function calcDistancia($lat1, $long1, $lat2, $long2)
    {
        $d2r = 0.01745292519943295769236;

        $dlong = ($long2 - $long1) * $d2r;
        $dlat = ($lat2 - $lat1) * $d2r;

        $temp_sin = sin($dlat/2.0);
        $temp_cos = cos($lat1 * $d2r);
        $temp_sin2 = sin($dlong/2.0);

        $a = ($temp_sin * $temp_sin) + ($temp_cos * $temp_cos) * ($temp_sin2 * $temp_sin2);
        $c = 2.0 * atan2(sqrt($a), sqrt(1.0 - $a));

        return 5358.1 * $c;
    }

    public function get_proximas($dados, $emergencia = FALSE)
    {                
        if (!isset($dados['distancia'])) {
            $dados['distancia'] = 10;
        }
        $msg = array('status' => FALSE, 'message' => 'Erro ao listar oficinas! Tente novamente!');
        $empty = array('status' => FALSE, 'message' => 'Nenhuma oficina encontrada dentro do raio informado!');
        $this->db->select('comercio.*, usuario.destaque as feature, endereco.*, usuario.telefone, usuario.celular, usuario.telEmergencia, usuario.email');
        $this->db->from('comercio');
        $this->db->join('usuario', 'comercio.idUsuario = usuario.idUsuario', 'left');
        $this->db->join('endereco', 'comercio.idComercio = endereco.idComercio', 'left');
        if ($emergencia == TRUE) {
            $this->db->where('comercio.planoEmergencia', 'Sim');
        }
        $this->db->order_by('usuario.destaque');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $clientes = $query->result();
                                
            foreach ($clientes as $key => $cliente) {
                if ($cliente->latitude == NULL || $cliente->longitude == NULL) {
                    unset($clientes[$key]);
                } else {
                    $distancia = $this->calcDistancia($dados['latitude'], $dados['longitude'], $cliente->latitude, $cliente->longitude);
                    if ($distancia >= $dados['distancia']) {
                        unset($clientes[$key]);
                    } else {
                        $cliente->status = TRUE;
                        $cliente->fotoDaEmpresa = base_url() . 'common/data/' . $cliente->fotoDaEmpresa;
                        $cliente->distancia = round($distancia, 2) . ' km';
                    }
                }
            }
            if (count($clientes) > 0) {
                return $clientes;
            } else {
                return $empty;
            }
        } else {
            return $empty;
        }
    }

    public function get_historico_usuario($id)
    {
        $msg = array('status' => FALSE, 'message' => 'Não há histórico para esse usuário!');
        $this->db->where('idUsuario', $id);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('servicos_agendados');
        if ($query->num_rows() > 0) {
            $historico = $query->result();
            foreach ($historico as $item) {
                $idComServ = $item->idComServ;
                $this->db->where('id', $idComServ);
                $item->idComercio = $this->db->get('servico_usuario')->row()->idUsuario;
                $this->db->where('idComercio', $item->idComercio);
                $item->razao = $this->db->get('comercio')->row()->razao;
                $this->db->where('id', $idComServ);
                $item->idServico = $this->db->get('servico_usuario')->row()->idServico;
                $this->db->where('idServico', $item->idServico);
                $item->servico = $this->db->get('servico')->row()->servico;
            }
            return $historico;
        } else {
            return $msg;
        }
        return $msg;
    }

    public function filtra_historico($dados)
    {
        $msg = array('status' => FALSE, 'message' => 'Não há histórico para esse usuário!');
        $error = array('status' => FALSE, 'message' => 'Erro ao filtrar o histórico! Tente novamente!');
        if (isset($dados['idUsuario']) && !empty($dados['idUsuario'])) {
            $this->db->where('idUsuario', $dados['idUsuario']);
            if (isset($dados['data_de']) && !empty($dados['data_de'])) {
                $this->db->where('data >=', $dados['data_de']);
            }

            if (isset($dados['data_ate']) && !empty($dados['data_ate'])) {
                $this->db->where('data <=', $dados['data_ate']);
            }
            $this->db->order_by('id', 'desc');
            $query = $this->db->get('servicos_agendados');
            if ($query->num_rows() > 0) {
                $historico = $query->result();
                foreach ($historico as $item) {
                    $idComServ = $item->idComServ;
                    $this->db->where('id', $idComServ);
                    $item->idComercio = $this->db->get('servico_usuario')->row()->idUsuario;
                    $this->db->where('idComercio', $item->idComercio);
                    $item->razao = $this->db->get('comercio')->row()->razao;
                    $this->db->where('id', $idComServ);
                    $item->idServico = $this->db->get('servico_usuario')->row()->idServico;
                    $this->db->where('idServico', $item->idServico);
                    $item->servico = $this->db->get('servico')->row()->servico;
                }
                if (isset($dados['idServico']) && !empty($dados['idServico'])) {
                    foreach ($historico as $key => $item) {
                        if($item->idServico != $dados['idServico']) {
                            unset($historico[$key]);
                        }
                    }
                }
                return $historico;
            } else {
                return $msg;
            }
            return $msg;
        } else {
            return $error;
        }
    }

    public function login($dados)
    {
        $msg = array('status' => FALSE, 'message' => 'Usuário ou senha inválido!');
        if (isset($dados['email'])) {
            $this->db->where('email', $dados['email']);
            $query = $this->db->get('usuario');
            if ($query->num_rows() == 1) {
                $usuario = $query->row();
                if ($usuario->senha == $dados['senha']) {
                    $usuario->status = TRUE;
                    if (!empty($usuario->foto)) {
                        $usuario->foto = base_url() . 'common/data/' . $usuario->foto;
                    }
                    return $usuario;
                } else {
                    return $msg;
                }
            } else {
                return $msg;
            }
        } else {
            if (isset($dados['provider_id'])) {
                $this->db->where('provider_id', $dados['provider_id']);
                $query = $this->db->get('usuario');
                if ($query->num_rows() == 1) {
                    $usuario = $query->row();
                    $usuario->status = TRUE;
                    $usuario->foto = base_url() . 'common/data/' . $usuario->foto;
                    
                    return $usuario;
                } else {
                    return $msg;
                }
            } else {
                return $msg;
            }
        }
    }

    public function set_status_push($dados)
    {
        $this->db->where('idUsuario', $dados['id']);
        $this->db->set('status_push', $dados['status']);
        if ($this->db->update('usuario')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function set_status_email($dados)
    {
        $this->db->where('idUsuario', $dados['id']);
        $this->db->set('status_email', $dados['status']);
        if ($this->db->update('usuario')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function set_status_sms($dados)
    {
        $this->db->where('idUsuario', $dados['id']);
        $this->db->set('status_sms', $dados['status']);
        if ($this->db->update('usuario')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function verifica_servico_concluido($dados)
    {
        $flag = FALSE;

        $this->db->where('idUsuario', $dados['idUsuario']);
        $this->db->where('status', 'Concluido');
        $this->db->where('`idComServ` in (select `id` from `servico_usuario` where `idUsuario` = ' . $dados['idCliente'] . ')', NULL, FALSE);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('servicos_agendados');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
        return FALSE;
    }

    public function get_marcas()
    {
        $this->db->select('id, nome');
        $this->db->order_by('nome', 'ASC');
        return $this->db->get('marca')->result();
    }

    public function get_modelos($id){
        $this->db->select('id, nome');
        $this->db->where('marca', $id);
        $this->db->order_by('nome', 'ASC');
        return $this->db->get('modelo')->result();
    }

    public function get_anos($id){
        $this->db->select('ano');
        $this->db->where('modelo', $id);
        $this->db->order_by('ano', 'ASC');
        return $this->db->get('ano_modelo')->result();
    }

    public function cadastrar_carro($dados)
    {
        if($this->db->insert('carro', $dados)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function excluir_carro($id)
    {
        $this->db->where('idCarro', $id);
        if($this->db->delete('carro')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function listar_carros($id){
        $this->db->where('idUsuario', $id);
        $this->db->order_by('idCarro', 'ASC');
        return $this->db->get('carro')->result();
    }

    public function get_datas($id)
    {
        $msg = array('status' => FALSE, 'message' => 'Erro ao listar as datas dessa oficina! Tente novamente!');
        $empty = array('status' => FALSE, 'message' => 'Não há datas disponíveis nessa oficina!');
        $this->db->select('idComercio, data');
        $this->db->where('idComercio', $id);
        $query = $this->db->get('agendamento');
        if ($query->num_rows() > 0) {
            $horarios = $query->result();
            return $horarios;
        } else {
            return $empty;
        }
        return $msg;
    }

    public function get_servicos_data($dados)
    {
        $msg = array('status' => FALSE, 'message' => 'Erro ao listar os horários dessa oficina! Tente novamente!');
        $empty = array('status' => FALSE, 'message' => 'Não há horários disponíveis nessa oficina!');
        $this->db->select('agendamento.*, servico.servico');
        $this->db->from('agendamento');
        $this->db->join('servico', 'agendamento.idServico = servico.idServico', 'left');
        $this->db->where('agendamento.idComercio', $dados['idComercio']);
        $this->db->where('agendamento.data', $dados['data']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $horarios = $query->result();
            foreach ($horarios as $horario) {
                $horario->status = TRUE;
                $this->db->where('idUsuario', $horario->idComercio);
                $this->db->where('idServico', $horario->idServico);
                $join = $this->db->get('servico_usuario')->row();
                $horario->idComServ = $join->id;
            }
            return $horarios;
        } else {
            return $empty;
        }
        return $msg;
    }

    public function verifica_disponibilidade($id)
    {
        $this->db->where('idComercio', $id);
        $qtd = $this->db->get('agendamento')->num_rows();
        if ($qtd > 0) {
            return TRUE;
        } else {
            return FALSE;
        } 
    }

    public function reportar($dados)
    {
        if ($this->db->insert('problema', $dados)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }   

    public function recuperar_senha($dados)
    {
        $this->db->where('email', $dados['email']);
        $query = $this->db->get('usuario');
        if ($query->num_rows() == 1) {
            return $query->row()->senha;
        } else {
            return NULL;
        }       
    }

    public function get_servicos_disponiveis($idOficina)
    {
        $msg = array('status' => FALSE, 'message' => 'Erro ao listar os serviços disponíveis dessa oficina! Tente novamente!');
        $empty = array('status' => FALSE, 'message' => 'Não há serviços disponíveis nessa oficina!');

        $this->db->where('`idServico` in (select `idServico` from `agendamento` where idComercio = ' . $idOficina . ')', NULL, FALSE);
        $query = $this->db->get('servico');
        if ($query->num_rows() > 0) {
            $servicos = $query->result();
            return $servicos;
        } else {
            return $empty;
        }
        return $msg;
    }

    public function get_datas_servico($dados)
    {
        $msg = array('status' => FALSE, 'message' => 'Erro ao listar os horários dessa oficina! Tente novamente!');
        $empty = array('status' => FALSE, 'message' => 'Não há horários disponíveis nessa oficina!');
        $this->db->select('agendamento.*, servico.servico');
        $this->db->from('agendamento');
        $this->db->join('servico', 'agendamento.idServico = servico.idServico', 'left');
        $this->db->where('agendamento.idComercio', $dados['idComercio']);
        $this->db->where('agendamento.idServico', $dados['idServico']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $datas = $query->result();
            foreach ($datas as $data) {
                $data->status = TRUE;
                $this->db->where('idUsuario', $data->idComercio);
                $this->db->where('idServico', $data->idServico);
                $join = $this->db->get('servico_usuario')->row();
                $data->idComServ = $join->id;
            }
            return $datas;
        } else {
            return $empty;
        }
        return $msg;
    }
}

/* End of file App_model.php */
/* Location: ./application/models/App_model.php */
