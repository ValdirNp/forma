<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mservico extends CI_Model {

	public function lista($array)
	{
		$query = $this->db
						->select('*')
						->where($array['where'])
						->get('servico');

		return array(
			'result' => $query->result(),
			'numrows' => $query->num_rows()
		);
	}

	public function lista_cliente($idComServ)
	{
		$this->db->where('id', $idComServ);
		$servico = $this->db->get('servico_usuario')->row()->idServico;

		$this->db->where('idServico', $servico);
		$query = $this->db->get('servico');

		return array(
			'result' => $query->result(),
			'numrows' => $query->num_rows()
		);
	}

	public function cadastrar($array)
	{
		if (isset($array['idServico'])) {
			$query = $this->db
							->where('idServico', $array['idServico'])
							->update('servico', $array);
		} else {
			$query = $this->db
							->insert('servico', $array);
			$id = $this->db->insert_id();
		}

		return array(
			'status' => $query,
			'id' => $id
		);
	}

}

/* End of file Mservico.php */
/* Location: ./application/models/Mservico.php */