<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfidelidade extends CI_Model {

    public function cadastra_pontos($dados)
    {
        $this->db->where('idAtendimento', $dados['idAtendimento']);
        $query = $this->db->get('fidelidade');
        if ($query->num_rows() == 0) {
            $this->db->insert('fidelidade', $dados);
        } else {
            $this->db->where('idAtendimento', $dados['idAtendimento']);
            $this->db->update('fidelidade', $dados);
        }
    }
}

/* End of file Mfidelidade.php */
/* Location: ./application/models/Mfidelidade.php */