<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcs extends CI_Model {

	public function cadastrar($array)
	{
		$query = $this->db
						->insert('comercio_servico', $array);

		return array(
			'status' => $query
		);
	}

	public function lista($id)
	{
		$query = $this->db
						->select('comercio.razao, servico_usuario.id, servico_usuario.dataHora, servico.servico')
						->join('comercio', 'comercio.idComercio = servico_usuario.idUsuario')
						->join('servico', 'servico.idServico = servico_usuario.idServico')
						->where('servico_usuario.idUsuario', $id)
						->get('servico_usuario');

		return array(
			'result' => $query->result(),
			'numrows' => $query->num_rows()
		);
	}

	public function excluir($id)
	{
		$query = $this->db
						->where('id', $id)
						->delete('comercio_servico');

		return array(
			'status' => $query
		);
	}
}

/* End of file Mcs.php */
/* Location: ./application/models/Mcs.php */