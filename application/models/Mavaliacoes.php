<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mavaliacoes extends CI_Model {

	public function lista($array)
	{
		$this->db->select('A.*, B.nome as usuario, C.nome as cliente');
        $this->db->from('avaliacoes as A');
        $this->db->join('usuario as B', 'A.idUsuario = B.idUsuario', 'left');
        $this->db->join('usuario as C', 'A.idCliente = C.idUsuario', 'left');
        $this->db->where($array);
        if ($this->session->userdata('tipo') == 'Cliente') {
            $this->db->where('A.idCliente', $this->session->userdata('idUsuario'));
        }
        $query = $this->db->get();

        return $query->result();
	}

    public function mudar_status($status, $id)
    {
        $this->db->where('id', $id);
        $this->db->set('status', $status);
        $this->db->update('avaliacoes');
    }


    public function cadastrar($array)
    {
        $query = $this->db
                        ->insert_batch('agendamento', $array); 

        return array(
            'status' => $query
        );
    }

    public function excluir($id)
    {
        $query = $this->db
                        ->where('idAgendamento', $id)
                        ->delete('agendamento');

        return array(
            'status' => $query
        );
    }
}