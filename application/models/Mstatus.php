<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mstatus extends CI_Model {

	public function lista($array)
	{
		$query = $this->db
						->select('*')
						->get('status');

		return array(
            'result'  => $query->result(),
            'numrows' => $query->num_rows()
        );
	}

}

/* End of file Mstatus.php */
/* Location: ./application/models/Mstatus.php */