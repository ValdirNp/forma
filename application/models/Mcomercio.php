<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcomercio extends CI_Model {

	public function cadastrar($array)
	{
		if (isset($array['idComercio'])) {
			$query = $this->db
							->where('idComercio', $array['idComercio'])
							->update('comercio', $array);

			$idComercio = $array['idComercio'];
		} else {
			$query = $this->db
							->insert('comercio', $array);

			$idComercio = $this->db->insert_id();
		}	

		return array(
            'status' => $query,
            'queryId' => $idComercio
        );
	}

	public function lista($array)
	{
		$query = $this->db
						->select('*')
						->where($array['where'])
						->get('comercio')
						->result();

		return array(
			'result' => $query
		);
	}

	public function lista_cliente($idComServ)
	{
		$this->db->where('id', $idComServ);
		$comercio = $this->db->get('servico_usuario')->row()->idUsuario;
		
		$this->db->where('idComercio', $comercio);
		$query = $this->db->get('comercio')->result();

		return array(
			'result' => $query
		);
	}

	public function meuComercio()
	{
		$query = $this->db
						->select('razao, idComercio')
						->join('comercio', 'comercio.IdUsuario = usuario.idUsuario')
						->where('usuario.IdUsuario', $this->session->userdata('idUsuario'))
						->where('usuario.idStatus', 1)
						->get('usuario');

		return array(
			'result' => $query->result(),
			'numrows' => $query->num_rows()
		);
	}

	public function busca_servicos($idComercio)
	{
		$this->db->where('`servico`.`idServico` in (select `idServico` from `servico_usuario` where `idUsuario` = ' .$idComercio. ')', NULL, FALSE);
        $query = $this->db->get('servico');
        return $query->result();
	}
}

/* End of file Mcomercio.php */
/* Location: ./application/models/Mcomercio.php */