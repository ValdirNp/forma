<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msu extends CI_Model {

	public function lista($array)
	{
		$query = $this->db
						->select('*')
						// ->join('comercio', 'comercio.idComercio = usuario_comercio.idComercio')
						// ->join('usuario', 'usuario.idUsuario = comercio.idUsuario')
						->where($array['where'])
						->get('servico_usuario');

		return array(
			'result' => $query->result(),
			'numrows' => $query->num_rows()
		);
	}

	public function cadastrar($array)
	{
		$query = $this->db
						->insert('servico_usuario', $array);
		return array(
			'status' => $query
		);
	}

    public function deleteByidUser($id)
    {
        $query = $this->db
                        ->where('idUsuario', $id)
                        ->delete('servico_usuario');

        return array(
            'status' => $query
        );
    }

    public function delete($id)
    {
    	$query = $this->db
    					->where('id', $id)
    					->delete('servico_usuario');

        return array(
            'status' => $query
        );
    }

}

/* End of file Muc.php */
/* Location: ./application/models/Muc.php */