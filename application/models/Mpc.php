<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpc extends CI_Model {

	public function lista($array)
	{
        $this->db->select('*');
        $this->db->from('problema_comentario as PC');
        $this->db->join('problema as P', 'PC.idProblema = P.idProblema', 'inner');
        $this->db->join('usuario as U', 'PC.idUsuario = U.idUsuario', 'inner');
        $this->db->where($array);
        $query = $this->db->get();

        return $query->result();
	}

	public function cadastrar($array)
	{
		$query = $this->db
						->insert('problema_comentario', $array);
		return array(
			'status' => $query
		);
	}

}

/* End of file Mpc.php */
/* Location: ./application/models/Mpc.php */