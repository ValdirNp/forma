<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mendereco extends CI_Model {

	public function cadastrar($array)
	{
		if (isset($array['idEndereco'])) {
			$query = $this->db
							->where('idEndereco', $array['idEndereco'])
							->update('endereco', $array);

			$idEndereco = $array['idEndereco'];
		} else {
			$query = $this->db
							->insert('endereco', $array);

			$idEndereco = $this->db->insert_id();
		}

		return array(
            'status' => $query,
            'queryId' => $idEndereco
        );
	}

	public function lista($array)
	{
		$query = $this->db
						->select('*')
						->where($array['where'])
						->get('endereco')
						->result();

		return array(
			'result' => $query
		);
	}
}

/* End of file Mendereco.php */
/* Location: ./application/models/Mendereco.php */