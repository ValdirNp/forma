<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcarro extends CI_Model {

	public function lista($id)
	{
        $query = $this->db
                        ->select('*')
                        ->where('idUsuario', $id)
                        ->get('carro');
                        
        return array(
            'result'  => $query->result(),
            'numrows' => $query->num_rows()
        );
	}

    public function get_marcas(){
        $this->db->select('id, nome');
        $this->db->order_by('nome', 'ASC');
        return $this->db->get('marca')->result();
    }

    public function get_modelos($id){
        $this->db->select('id, nome');
        $this->db->where('marca', $id);
        $this->db->order_by('nome', 'ASC');
        return $this->db->get('modelo')->result();
    }

    public function get_anos($id){
        $this->db->select('ano');
        $this->db->where('modelo', $id);
        $this->db->order_by('ano', 'ASC');
        return $this->db->get('ano_modelo')->result();
    }

    public function cadastrar($array)
    {
        $query = $this->db
                    	->insert('carro', $array);

        return array(
            'status' => $query
        );
    }

    public function delete($id)
    {
    	$query = $this->db
    					->where('idCarro', $id)
    					->delete('carro');

        return array(
            'status' => $query
        );
    }

}

/* End of file Mcarro.php */
/* Location: ./application/models/Mcarro.php */
