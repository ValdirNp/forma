<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproblema extends CI_Model {

	public function lista($array)
	{
        $this->db->select('*');
        $this->db->from('problema as P');
        $this->db->join('usuario as U', 'P.idUsuario = U.idUsuario', 'left');
        $this->db->where($array);
        $query = $this->db->get();

        return $query->result();
	}

	public function cadastrar($array)
	{
		$query = $this->db
						->insert('servico_usuario', $array);
		return array(
			'status' => $query
		);
	}

    public function deleteByidUser($id)
    {
        $query = $this->db
                        ->where('idUsuario', $id)
                        ->delete('servico_usuario');

        return array(
            'status' => $query
        );
    }

    public function delete($id)
    {
    	$query = $this->db
    					->where('id', $id)
    					->delete('servico_usuario');

        return array(
            'status' => $query
        );
    }

}

/* End of file Mproblema.php */
/* Location: ./application/models/Mproblema.php */