<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Magendamento extends CI_Model {

	public function lista($array)
	{
        if ($this->session->userdata('tipo') == 'Cliente') {
            $clienteID = $this->session->userdata('idUsuario');
            $this->db->select('*');
            $this->db->from('servicos_agendados');
            $this->db->where('status !=', 'Cancelado');
            $this->db->where('`idComServ` in (select `id` from `servico_usuario` where `idUsuario` in (select `idComercio` from `comercio` where `IdUsuario` = ' .$clienteID. '))', NULL, FALSE);
            $this->db->order_by('hora', 'ASC');
            $query = $this->db->get();
        } else {            
            $query = $this->db
            ->select('*')
            ->where($array)
            ->get('agendamento');
        }

        return array(
            'result'  => $query->result(),
            'numrows' => $query->num_rows()
        );
    }

    public function lista_agendamentos()
    {
        $this->db->select('servicos_agendados.*, usuario.*');
        $this->db->from('servicos_agendados');
        $this->db->join('usuario', 'servicos_agendados.idUsuario = usuario.idUsuario', 'left');
        $this->db->where('servicos_agendados.status !=', 'Cancelado');
        if ($this->session->userdata('tipo') == 'Cliente') {
            $clienteID = $this->session->userdata('idUsuario');
            $this->db->where('`servicos_agendados`.`idComServ` in (select `id` from `servico_usuario` where `idUsuario` in (select `idComercio` from `comercio` where `idUsuario` = ' . $clienteID . '))', NULL, FALSE);
        }
        $this->db->order_by('servicos_agendados.dataHora', 'desc');
        $query = $this->db->get();
        return array(
            'result'  => $query->result(),
            'numrows' => $query->num_rows()
        );
    }

    public function periodo()
    {
        $result = array();
        for ($i=1; $i <= 12; $i++) { 
            $this->db->where('MONTH(data)', $i);
            $result[$i] = $this->db->get('agendamento')->num_rows();
        }
        return $result;
    }

    public function cadastrar($array)
    {
        $query = $this->db
        ->insert_batch('agendamento', $array); 

        return array(
            'status' => $query
            );
    }

    public function excluir($id)
    {
        $query = $this->db
        ->where('idAgendamento', $id)
        ->delete('agendamento');

        return array(
            'status' => $query
            );
    }

    /* draws a calendar */
    function draw_calendar($clienteID, $month, $year)
    {
        if ($this->session->userdata('tipo') == 'Cliente') {
            $clienteID = $this->session->userdata('idUsuario');
        }
        /* draw table */
        $calendar = '<table id="calendario" class="calendar table table-bordered" data-year="' . $year . '" data-month="' . $month . '">';

        /* table headings */
        $headings = array('Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado');
        // $calendar.= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';
        $calendar.= '<thead><tr class="calendar-row"><th class="calendar-day-head">'.implode('</th><th class="calendar-day-head">',$headings).'</th></tr></thead>';
        $calendar.= '<tbody>';

        /* days and weeks vars now ... */
        $running_day = gmdate('w',mktime(0,0,0,$month,1,$year));
        $days_in_month = gmdate('t',mktime(0,0,0,$month,1,$year));
        $days_in_this_week = 1;
        $day_counter = 0;
        $dates_array = array();

        /* row for week one */
        $calendar.= '<tr class="calendar-row">';

        /* print "blank" days until the first of the current week */
        for($x = 0; $x < $running_day; $x++):
            $calendar.= '<td class="calendar-day-np"> </td>';
            $days_in_this_week++;
        endfor;

        /* keep going with days.... */
        for($list_day = 1; $list_day <= $days_in_month; $list_day++):
            $calendar.= '<td class="calendar-day">';
            // add in the day number 
            $calendar.= '<div class="day-number">'.$list_day.'</div>';

            /** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
            $this->db->select('*');
            $this->db->from('servicos_agendados');
            $this->db->where('DAY(data)', $list_day);
            $this->db->where('MONTH(data)', $month);
            $this->db->where('YEAR(data)', $year);
            $this->db->where('status !=', 'Cancelado');
            $this->db->where('`idComServ` in (select `id` from `servico_usuario` where `idUsuario` in (select `idComercio` from `comercio` where `IdUsuario` = ' .$clienteID. '))', NULL, FALSE);
            $this->db->order_by('hora', 'ASC');
            $agendamentos = $this->db->get();

            if ($agendamentos->num_rows() != 0) {
                $calendar .= '<select name="" class="select form-control busy">';
                $calendar .= '<option value="0" selected>Agendados</option>';
                foreach ($agendamentos->result() as $agendamento) {
                    $this->db->where('idUsuario', $agendamento->idUsuario);
                    $name = $this->db->get('usuario')->row()->nome;
                    $calendar .= '<option value="' . $agendamento->id . '">' . gmdate('H:i', strtotime($agendamento->hora)) . ' - ' . $name . '</option>';
                }
                $calendar .= '</select>';
            }

            $this->db->where('DAY(data)', $list_day);
            $this->db->where('MONTH(data)', $month);
            $this->db->where('YEAR(data)', $year);
            $this->db->where('`idComercio` in (select `idComercio` from `comercio` where `IdUsuario` = ' .$clienteID. ')', NULL, FALSE);
            $this->db->order_by('horario', 'ASC');
            $livres = $this->db->get('agendamento');
            if ($livres->num_rows() != 0) {
                $calendar .= '<select name="" class="select form-control free">';
                $calendar .= '<option value="0" selected>Disponíveis</option>';
                foreach ($livres->result() as $livre) {
                    $calendar.= '<option value="">' . date('H:i', strtotime($livre->horario)) . ' - LIVRE</option>';
                }
                $calendar .= '</select>';
            }

            $calendar.= '</td>';
            if($running_day == 6):
                $calendar.= '</tr>';
                if(($day_counter+1) != $days_in_month):
                    $calendar.= '<tr class="calendar-row">';
                endif;
                $running_day = -1;
                $days_in_this_week = 0;
            endif;
            $days_in_this_week++; $running_day++; $day_counter++;
        endfor;

        /* finish the rest of the days in the week */
        if($days_in_this_week < 8):
            for($x = 1; $x <= (8 - $days_in_this_week); $x++):
                $calendar.= '<td class="calendar-day-np"> </td>';
            endfor;
        endif;

        /* final row */
        $calendar.= '</tr>';

        $calendar.= '</tbody>';

        /* end the table */
        $calendar.= '</table>';

        /* all done, return result */
        return $calendar;
    }   

    function get_agendamento_periodo_charts(){
        $query = "select DATE(data) as data, count(data) as qtd from servicos_agendados where DATE(data) between '".gmdate('Y-m-'.(gmdate('d')-6))."' AND '".gmdate('Y-m-d')."' group by data order by data";
    // $query = "select data, count(data) as qtd from agendamento where data between '2015-07-07' and '2015-07-22'  group by data order by data";
        return $this->db->query($query)->result();

    } 
    function get_agendamento(){
        $query = "select dataHora as data, count(dataHora) as qtd from servicos_agendados where DATE(dataHora) between '".gmdate('Y-m-'.(gmdate('d')-6))."' AND '".gmdate('Y-m-d')."' group by dataHora order by dataHora";
        return $this->db->query($query)->result();        
    }  

    public function get_detalhes($id)
    {
        $this->db->where('id', $id);
        $agendamento = $this->db->get('servicos_agendados')->row();
        $agendamento->hora = gmdate('H:i', strtotime($agendamento->hora));
        $agendamento->data = gmdate('d/m/Y', strtotime($agendamento->data));
        $this->db->where('idUsuario', $agendamento->idUsuario);
        $agendamento->nome = $this->db->get('usuario')->row()->nome;

        $servcom = $this->db->get_where('servico_usuario', array('id' => $agendamento->idComServ))->row();
        $agendamento->servico = $this->db->get_where('servico', array('idServico' => $servcom->idServico))->row()->servico;

        return $agendamento;
    }

    public function atualizar_status($id, $status)
    {
        $this->db->where('id', $id);
        $this->db->set('status', $status);
        if ($this->db->update('servicos_agendados')) {
            return TRUE;
        }
        return FALSE;
    }

    public function atualizar_info($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('servicos_agendados', $data);
        $this->db->where('id', $id);
        return $this->db->get('servicos_agendados')->row();
    }
}

/* End of file Magendamento.php */
/* Location: ./application/models/Magendamento.php */