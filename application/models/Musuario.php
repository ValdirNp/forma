<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Musuario extends CI_Model {

    public function login($array)
    {
        $query = $this->db
        ->select('*')
        ->join('status', 'status.idStatus = usuario.idStatus')
        ->where('usuario.email', $array['email'])
        ->where('usuario.senha', $array['senha'])
        ->where('usuario.idStatus', 1)
        ->get('usuario');

        return array(
            'result'  => $query->result_array(),
            'numrows' => $query->num_rows()
            );
    }

    public function lista($array)
    {
        if (($this->session->userdata('tipo')) && ($this->session->userdata('tipo') == 'Cliente')) {
            $comercio = $this->db
            ->select('idComercio')
            ->where('IdUsuario', $this->session->userdata('idUsuario'))
            ->get('comercio')
            ->result();
            
            $query = $this->db
            ->select('*')
            ->join('usuario_comercio', 'usuario_comercio.idUsuario = usuario.idUsuario')
            ->where('usuario_comercio.idComercio', $comercio[0]->idComercio)
            ->get('usuario');

        } else {
            $query = $this->db
            ->select('*')
            ->join('usuario_comercio', 'usuario_comercio.idUsuario = usuario.idUsuario')
            ->where($array['where'])
            ->order_by('usuario.dataHora', 'DESC')
            ->get('usuario');
        }
        
        
        return array(
            'result'  => $query->result(),
            'numrows' => $query->num_rows()
            );
    }

    public function lista_clientes($status = NULL, $nivel = NULL)
    {
        $this->db->select('comercio.*, usuario.*');
        $this->db->from('comercio');
        $this->db->join('usuario', 'comercio.idUsuario = usuario.idUsuario', 'left');
        if ($status != NULL) {
            $this->db->where('usuario.idStatus', $status);
        }
        if ($nivel != NULL) {
            $this->db->where('usuario.nivel', $nivel);
        }
        
        $this->db->order_by('usuario.dataHora');
        $query = $this->db->get();
        
        return array(
            'result'  => $query->result(),
            'numrows' => $query->num_rows()
        );
    }

    public function lista_cliente($array)
    {
     
        $query = $this->db
        ->select('*')
        ->where($array['where'])
        ->order_by('dataHora', 'DESC')
        ->get('usuario');
        
        return array(
            'result'  => $query->result(),
            'numrows' => $query->num_rows()
            );
    }

    public function lista_usuarios()
    {
        $this->db->select('A.*, B.nome as cliente_cad');
        $this->db->from('usuario A');
        $this->db->join('usuario B', 'A.cliente = B.idUsuario');
        $this->db->where('A.nivel', 'Usuario');
        $this->db->where('A.idStatus', 1);
        $this->db->order_by('A.dataHora', 'desc');
        $query = $this->db->get();
        return array(
            'result'  => $query->result(),
            'numrows' => $query->num_rows()
        );
                
    }

    public function cadastrar($array)
    {
        if (isset($array['idUsuario'])) {
            $query = $this->db  
            ->where('idUsuario', $array['idUsuario'])
            ->update('usuario', $array);

            $idUsuario = $array['idUsuario'];
        } else {
            $query = $this->db
            ->insert('usuario', $array);

            $idUsuario = $this->db->insert_id();
        }

        return array(
            'status' => $query,
            'queryId' => $idUsuario
            );
    }

    public function verificarNivel($id)
    {
        $query = $this->db
        ->select('*')
        ->join('status', 'status.idStatus = usuario.idStatus')
        ->where('usuario.idUsuario', $id)
        ->where('usuario.idStatus', 1)
        ->get('usuario')
        ->result_array();

        return $query[0]['nivel'];
    }

    public function busca($search)
    {
        $this->db->where('nivel', 'Usuario');
        $this->db->like('nome', $search);
        $this->db->order_by('nome', 'ASC');
        return $this->db->get('usuario')->result();
    }

    public function get_bar_charts(){
        // $query = "select count(A.cliente) as quantidade, A.nome as cliente
        //           from usuario A inner join usuario B ON A.idUsuario = B.cliente
        //           where B.nivel = 'Cliente' AND A.dataHora between '".date('Y-m-'.(date('d')-6))."' AND '".date('Y-m-d')."' group by B.cliente";
        $query = "select count(A.idUsuario) as quantidade, A.nome 
        from usuario A inner join usuario B ON A.idUsuario = B.cliente
        where A.nivel = 'Cliente' 
        and (B.nivel = 'Usuario' or B.nivel = 'Root')
        and A.dataHora between '".gmdate('Y-m-'.(gmdate('d')-6))."' AND '".gmdate('Y-m-d')."'
        group by A.idUsuario";
                        // echo '<pre>';
                        // var_dump($query);
                        // exit;
        return $this->db->query($query)->result();

    }
    public function get_usuario_charts(){
        $query = "select DATE(dataHora) as data, count(dataHora) as qtd from usuario where DATE(dataHora) between '".gmdate('Y-m-'.(gmdate('d')-6))."' AND '".gmdate('Y-m-d')."' group by DATE(dataHora) order by dataHora";
        return $this->db->query($query)->result();

    }
    public function get_cliente_charts(){
        $query = "select DATE(dataHora) as data, count(dataHora) as qtd from usuario where nivel = 'Cliente' and DATE(dataHora) between '".gmdate('Y-m-'.(gmdate('d')-6))."' AND '".gmdate('Y-m-d')."' group by DATE(dataHora) order by dataHora";
        return $this->db->query($query)->result();

    }

    public function create_notification($array, $tipo, $reader=false){                   
                   
        //$tipo é quem vai ler, root ou cliente
        if($tipo == 'Root'){
            $roots = $this->db->query('select distinct idUsuario from usuario where nivel = "Root"')->result();
            foreach ($roots as $root) {
                $array['reader'] = $root->idUsuario;
                $this->db->insert('notificacao', $array);
            }

        }else{
            if($tipo == 'Cliente'){
                $array['reader'] = $reader;
                $this->db->insert('notificacao', $array);
            }
        }
    }

    public function lista_pagamentos($array)
    {
        $this->db->where($array['where']);
        $this->db->order_by('vencimento', 'ASC');
        $query = $this->db->get('pagamentos');

        return array(
            'result'  => $query->result(),
            'numrows' => $query->num_rows()
        );
    }

    public function atualizar_pagamento($vencimento, $pago, $idUsuario)
    {
        $this->db->set('vencimento', $vencimento);
        $this->db->set('pago', $pago);
        $this->db->set('idUsuario', $idUsuario);
        $this->db->insert('pagamentos');
    }

    public function comentarios($idUsuario)
    {
        $this->db->select('comentarios.*, usuario.nome as cliente');
        $this->db->from('comentarios');
        $this->db->join('usuario', 'comentarios.idCliente = usuario.idUsuario', 'left');
        $this->db->where('comentarios.idUsuario', $idUsuario);
        return $this->db->get()->result();
    }

    public function comentar($data)
    {
        $data['idCliente'] = $this->session->userdata('idUsuario');
        $this->db->insert('comentarios', $data);
    }    
}

/* End of file Musuario.php */
/* Location: ./application/models/Musuario.php */