jQuery(document).ready(function($) {
	console.log('cliente.js');

	// addTr
	$('.addTr').on('click', function(event) {
		event.preventDefault();

		var clicked = $(this),
			clickedParent = clicked.parent().parent().parent();
			clickedParent.parent().append(clickedParent.clone());

		$('.addTr').each(function(i, v) {
			if (i > 0) {
				$(this).removeClass('btn-success').addClass('btn-danger').find('.glyphicon-plus').removeClass('glyphicon-plus').addClass('glyphicon-minus');

				$('.glyphicon-minus').on('click', function(event) {
					event.preventDefault();
					$(this).parent().parent().parent().remove();
				});
			}
		});
	});
});